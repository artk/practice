var chai = require('chai'), expect = chai.expect;
var LLRBTree = require('../lib/LLRBTree.js');
var rbnode = require ('../lib/RBNode.js');

describe('LLRBTree Suite', function () {
    describe('Insert into one-node tree: ', function () {
        beforeEach(function () {
            this.T = new LLRBTree(function (a, b) { return a.localeCompare(b); });
        });

        it('should have a left red child and a black root after inserting a key',
            function () {
               var T = this.T;

               T.insert('b');  // root

               T.insert('a');  // left child
               expect(!T.root.isRed() && T.root.left.isRed()).to.be.true;
           });

        it('should have the root become a left red child after inserting a key larger than the root',
            function () {
                var T = this.T;

                T.insert('b'); // root

                T.insert('c'); // right child
                expect(!T.root.isRed() && T.root.key === 'c'
                    && T.root.left.isRed() && T.root.left.key == 'b').to.be.true;
            })
    });

    describe('Insert into a tree with two nodes', function () {
        beforeEach(function () {
            this.T = new LLRBTree(function (a, b) { return a.localeCompare(b); });
        });

        it('should have a root with two black children after inserting a key larger than the root',
            function () {
                var T = this.T;

                T.insert('b'); // root
                T.insert('a'); // left red child

                T.insert('c'); // right left child, triggers color-flip
                expect(T.root.key === 'b' && !T.root.left.isRed()
                      && !T.root.right.isRed()).to.be.true;
            });

        it('should have a new root with two black children after inserting a key smaller than the root\'s left child',
            function () {
                var T = this.T;

                T.insert('c'); // root
                T.insert('b'); // left red child

                T.insert('a'); // triggers a right-rotation with b as new root
                expect(T.root.key === 'b' && !T.root.left.isRed()
                      && !T.root.right.isRed()).to.be.true;

            });

        it('should have a new root with two black children after inserting a key larger than the root\'s left child',
            function () {
                var T = this.T;

                T.insert('c');  // root
                T.insert('a');  // left child

                T.insert('b');  // a's right child, triggers left-rotation,
                                // then a right rotation, and a color-flip

                expect(T.root.key === 'b' && !T.root.left.isRed()
                      && !T.root.right.isRed()).to.be.true;
            });

        it('should not have duplicate keys',
           function () {
            var T = this.T;

            T.insert('c');
            T.insert('b');
            T.insert('b');

            expect(T.inorder().filter(
                function (x, i, v){
                    if (i > 0 && v[i] === v[i-1]){
                        return true;
                    }
                    else {
                        return false;
                    }
                }).length).to.be.equal(0);

        });

    });

});

