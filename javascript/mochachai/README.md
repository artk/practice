##References##

### API Documentation ###
* [Mocha](http://visionmedia.github.io/mocha/#getting-started)
* [Chai](http://chaijs.com)

###Tutorial ###
* [Testing Frontend Javascript Code Using Mocha, Chai and Sinon](https://nicolas.perriault.net/code/2013/testing-frontend-javascript-code-using-mocha-chai-and-sinon/)

###Mocha Integration for Jetbrains products
* [Webstorm: Node.js Development Workflow](http://blog.jetbrains.com/webstorm/2014/02/webinar-recording-node-js-development-workflow/)
* [Webstorm 7 - Integration of Mocha Test Framework](http://www.youtube.com/watch?v=4mKiGkokyx8&feature=youtu.be&t=1m4s)
    * Instructions work for PyCharm 3.1
