var chai = require('chai'), expect = chai.expect;
var BST = require('../lib/BST.js');

describe('BST Suite', function () {
    beforeEach(function () {
        this.T = new BST(function (a, b) {
            return a - b;
        });
        var T = this.T;

        T.insert(5);
        T.insert(10);
        T.insert(7);
        T.insert(2);
        T.insert(4);
        T.insert(3);
        T.insert(15);
        T.insert(12);
        T.insert(11);
        T.insert(9);

    })

    describe('Constructor tests', function () {
        it('should throw if no argument is passed',
            function () {
                expect(function () {
                    new BST();
                }).to.throw(Error);
            });

        it('should throw if the argument passed is not a function',
            function () {
                expect(function () {
                    new BST("abc");
                }).to.throw(Error);
            });

        it('should be an instance of BST after creation',
            function () {
                var T = new BST(function (a, b) {
                    return a - b;
                });
                expect(T).to.be.an.instanceof(BST);
            });
    });


    describe('insert and isEmpty tests', function () {
        before(function () {
            this.T = new BST(function (a, b) {
                return a - b;
            });
        });

        it('should be an empty tree immediately after BST is created',
            function () {
                expect(this.T.isEmpty()).to.be.true;
            });

        it('should not be an empty tree if the tree has nodes',
            function () {
                this.T.insert(111);
                expect(this.T.isEmpty()).to.be.false;
            });
    });


    describe('search tests', function () {
        it('should find a node when passed a valid key', function () {
            expect(this.T.search(2)).to.be.an('object');
        });

        it('should not find a node when passed an invalid key', function () {
            expect(this.T.search(17)).to.be.a('null');
        });
    });


    describe('min and max tests', function () {
        it('should find 15 as the max node', function () {
            expect(this.T.max().key).to.equal(15);
        });

        it('should find 2 as the min node', function () {
            expect(this.T.min().key).to.equal(2);
        });

    });

    describe('parent tests', function () {

        it('should find 2 as the parent of 4', function () {
            expect(this.T.parent(this.T.search(4)).key).to.equal(2);
        });

        it('should find 15 as the parent of 12', function () {
            expect(this.T.parent(this.T.search(12)).key).to.equal(15);
        });

        it('should find root as the parent of root', function () {
            expect(this.T.parent(this.T.root)).to.deep.equal(this.T.root);
        });

    });

    describe('predecessor tests', function () {

        it('should find node with the key 2 as the predecessor of 3',
            function () {
                expect(this.T.predecessor(this.T.search(3))).
                    to.deep.equal(this.T.search(2));
            });

        it('should find node with the key 4 as the predecessor of root (key 5)',
            function () {
                expect(this.T.predecessor(this.T.root)).
                    to.deep.equal(this.T.search(4));
            });

        it('should find null as the predecessor of the node with min key',
            function () {
                expect(this.T.predecessor(this.T.min())).
                    to.be.a('null');
            });

    });

    describe('successor tests', function () {
        it('should find node 15 as the successor of node 12',
            function () {
                expect(this.T.successor(this.T.search(12))).
                    to.deep.equal(this.T.search(15));
            });

        it('should return node 7 as the successor of root node',
            function () {
                expect(this.T.successor(this.T.root)).
                    to.deep.equal(this.T.search(7));
            });

        it('should return null as the successor of the node with max key',
            function () {
                expect(this.T.successor(this.T.max())).
                    to.be.a('null');
            });
    });


    describe('delete tests', function () {

        it("should find parent of node with one less child if the node that was deleted is childless",
            function () {
                var T = this.T, key = 11, node = T.search(key); // 11 is childless
                var parent = T.parent(node),
                    isLeftChild = (parent.left === node) ? true : false;

                T.delete(key);

                if (isLeftChild) expect(parent.left).to.be.a('null');
                else expect(parent.right).to.be.a('null');
            })

        // Test case for node with one child (left)
        it("should find parent's right child replaced by its grandchild if this grandchild is an only child and a left child.",
            function () {
                var T = this.T, key = 4, node = T.search(key),
                    parent = T.parent(node), lChild = node.left;

                T.delete(key);
                expect(parent.right).to.deep.equal(lChild);
            })

        it("should find parent's left child replaced by its grandchild if this grandchild is an only child and a right child.",
            function () {
                var T = this.T, key = 7, node = T.search(key),
                    parent = T.parent(node), rChild = node.right;

                T.delete(key);
                expect(parent.left).to.deep.equal(rChild);
            })

        it("should find the node be replaced by its inorder successor node",
            function () {
                var T = this.T, key = T.root.key, node = T.root;  // root has two children
                var succ = T.successor(node), lChild = T.root.left,
                    rChild = T.root.right;

                T.delete(key);
                expect(T.root.key === succ.key &&
                    T.root.left === lChild &&
                    T.root.right === rChild).to.be.true;
            })

    });

    describe('deleteMin tests', function () {

        it('should find the new min to be the current min\'s successor, after deleteMin is applied',
            function () {
                var T = this.T, succ = T.successor(T.min());

                T.deleteMin();
                expect(T.min()).to.deep.equal(succ);
            });

        it('should find an empty tree after deleteMin on a single-node tree',
            function () {
                var T = new BST(function (a, b) {
                    return a - b;
                });

                T.insert(11);
                T.deleteMin();
                expect(T.isEmpty()).to.be.true;
            });
    });

    describe('deleteMax tests', function () {
        it('should find the new max to be the current max\'s predecessor, after deleteMin is applied',
            function () {
                var T = this.T, pred = T.predecessor(T.max());

                T.deleteMax();
                expect(T.max()).to.deep.equal(pred);
            });

        it('should find an empty tree after deleteMax on a single-node tree',
            function () {
                var T = new BST(function (a, b) {
                    return a - b;
                });

                T.insert(11);
                T.deleteMax();
                expect(T.isEmpty()).to.be.true;
            });
    });

});
