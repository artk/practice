var chai = require('chai'), expect = chai.expect;
var Heap = require('../lib/Heap.js');
var inequality = require('../lib/inequality.js');

describe("Heap Suite", function () {
    beforeEach(function () {
        var heap = new Heap();

        // create dummy methods for unit-tests
        heap.sink = function () {
        }; // do-nothing
        heap.swim = function () {
        }; // do-nothing

        this.heap = heap;
        this.compareFunction = function (a, b) { return a - b; };
    });

    it('should be empty after heap object is created',
        function () {
            expect(this.heap.isEmpty()).to.be.true;
        });

    it('should be size 0 after heap object is created',
        function () {
            expect(this.heap.size()).to.equal(0);
        });

    it('should not be empty after adding an element',
        function () {
            this.heap.insert(10);
            expect(this.heap.isEmpty()).to.be.false;
        });

    it('should be size 1 after adding an element',
        function () {
            this.heap.insert(10);
            expect(this.heap.size()).to.equal(1);
        });

    it('should be size 3 after adding three elements',
        function () {
            var heap = this.heap;

            heap.insert(1);
            heap.insert(2);
            heap.insert(3);
            expect(heap.size()).to.equal(3);
        });

    it ('should be empty after removing every element from the heap',
        function () {
            var heap = this.heap;

            heap.insert(1);
            heap.insert(2);
            heap.insert(3);
            heap.extract();
            heap.extract();
            heap.extract();
            expect(heap.isEmpty()).to.be.true;
        });

    it('should be size 0 after removing every element from the heap',
        function () {
            var heap = this.heap;

            heap.insert(1);
            heap.insert(2);
            heap.insert(3);
            heap.extract();
            heap.extract();
            heap.extract();
            expect(heap.size()).to.equal(0);
        });

    it('should exchange two elements in an array',
        function () {
            var arr = [1, 2], before0 = arr[0], before1 = arr[1];

            Heap.exchange(arr, 0, 1);
            expect(arr[0] === before1 && arr[1] === before0).to.be.true;
        });

    it ('should have a left child whose index is twice the node\'s index + 1',
        function () {
            expect(Heap.left(5)).to.be.equal(11);
        });


    it ('should have a right child whose index is twice the node\'s index + 2',
        function () {
            expect(Heap.right(5)).to.be.equal(12);
        });


    it ('should have children with higher key values after calling heapify',
        function () {
            var A, less;

            less = inequality.less(this.compareFunction);
            A = [8, 9, 5, 7, 1, 2, 12, 14, 4, 3];
            Heap.heapify(A, 1, A.length-1, less);
            expect(A[1] <= A[3] && A[1] <= A[4] && A[1] === 1).to.be.true;
        });

    it ('should have children with lower key values after calling heapify',
        function () {
            var A, greater;

            greater = inequality.greater(this.compareFunction);
            A = [8, 9, 5, 7, 1, 2, 12, 14, 4, 3];
            Heap.heapify(A, 3, A.length-1, greater);
            expect(A[3] >= A[7] && A[3] >= A[8] && A[3] === 14).to.be.true;
        });

    it ('should return the first element of heap after calling peek',
        function () {
            this.heap.insert(5);

            expect(this.heap.peek() === this.heap.heap[0]).to.be.true;
        });

    it ('should throw if peek is called on an empty heap',
        function () {
            var h = this.heap;
            expect(function () { return h.peek(); }).to.throw(Error);
        });

    it('should make a min heap after calling makeHeap', function () {
        var A, less, last, isHeap;

        A = [8, 9, 5, 7, 1, 2, 12, 14, 4, 3];
        less = inequality.less(this.compareFunction);
        Heap.makeHeap(A, less);
        last = A.length -1;

        isHeap = A.every(function (item, k, a) {
            var l = Heap.left(k), r = Heap.right(k), pass;

            if (l > last) {
                return true;
            }

            pass = a[k] <= a[l];

            if (r > last) {
                return pass;
            }

            return pass && a[k] <= a[r];
        });

        expect(isHeap).to.be.true;
    });

    it('should make a max heap after calling makeHeap', function () {
        var A, greater, last, isHeap;

        A = [8, 9, 5, 7, 1, 2, 12, 14, 4, 3];
        greater = inequality.greater(this.compareFunction);
        Heap.makeHeap(A, greater);
        last = A.length -1;

        isHeap = A.every(function (item, k, a) {
            var l = Heap.left(k), r = Heap.right(k), pass;

            if (l > last) {
                return true;
            }

            pass = a[k] >= a[l];

            if (r > last) {
                return pass;
            }

            return pass && a[k] >= a[r];
        });

        expect(isHeap).to.be.true;
    });

});
