var chai = require('chai'), expect = chai.expect;
var MaxHeap = require('../lib/MaxHeap.js');
var inequality = require('../lib/inequality.js');

describe("MaxHeap Suite", function () {
    beforeEach(function () {
        this.compareFunction = function (a, b) { return a - b; };
    });

    it('should throw if no argument is passed in to the constructor',
        function () {
            expect(function () {
                var h = new MaxHeap();
            }).to.throw(Error);
        });

    it('should throw if the argument passed to the constructor is not a function',
        function () {
            expect(function () {
                var h = new MaxHeap("abc");
            }).to.throw(Error);
        });

    it("should throw if the constructor's list argument is not an array",
        function () {
            expect(function () {
                var x = new MaxHeap(function () {}, "abc");
            }).to.throw(Error);
        });

    it('should be an instance of MaxHeap after constructor completes successfully',
        function () {
            var h = new MaxHeap(this.compareFunction);
            expect(h).to.be.an.instanceof(MaxHeap);
        });


    it('should always return the maximum each time extract is called on a non-empty heap',
        function () {
            var h, prev, curr, higherOrEqual;

            h = new MaxHeap(this.compareFunction,
                            [11, 7, 13, 9, 3, 17, 2, 21, 5, 19]);

            prev = h.peek();
            higherOrEqual = true;

            while (!h.isEmpty()) {
                curr = h.extract();
                higherOrEqual = higherOrEqual &&(prev >= curr);
                prev = curr;
            }

            expect(higherOrEqual).to.be.true;
        });

    it ('should have an array in ascending order after calling sort',
        function () {
            var prev, A;

            A = [5, 10, 25, 6, 13, 101, 72, 11, 3, 1, 19, 13, 22, 42, 47, 2, 5];
            MaxHeap.sort(A, this.compareFunction);
            prev = A[0];

            expect(A.every(function (curr) {
                var cmp = (prev <= curr);
                prev = curr;
                return cmp;
            })).to.be.true;
        });
});
