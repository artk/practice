var chai = require('chai'), expect = chai.expect;
var MinHeap = require('../lib/MinHeap.js');
var inequality = require('../lib/inequality.js');

describe("MinHeap Suite", function () {
    beforeEach(function () {
        this.compareFunction = function (a, b) { return a - b; };
    });

    it('should throw if no argument is passed in to the constructor',
        function () {
            expect(function () {
                var x = new MinHeap();
            }).to.throw(Error);
        });

    it("should throw if the constructor's compareFunction is not a function",
        function () {
            expect(function () {
                var x = new MinHeap("abc");
            }).to.throw(Error);
        });

    it("should throw if the constructor's list argument is not an array",
        function () {
            expect(function () {
                var x = new MinHeap(function () {}, "abc");
            }).to.throw(Error);
        });

    it('should be an instance of MinHeap after constructor completes successfully',
        function () {
            var h = new MinHeap(function (a, b) {
                return a - b;
            });
            expect(h).to.be.an.instanceof(MinHeap);
        });

    it('should always return the minimum each time extract is called on a non-empty heap',
        function () {
            var h, prev, curr, lowerOrEqual;

            h = new MinHeap(this.compareFunction,
                            [11, 7, 13, 9, 3, 17, 2, 21, 5, 19]);

            prev = h.peek();
            lowerOrEqual = true;

            while (!h.isEmpty()) {
                curr = h.extract();
                lowerOrEqual = lowerOrEqual &&(prev <= curr);
                prev = curr;
            }

            expect(lowerOrEqual).to.be.true;
        });

    it ('should have an array in descending order after calling sort',
        function () {
            var prev, A;

            A = [5, 10, 25, 6, 13, 101, 72, 11, 3, 1, 19, 13, 22, 42, 47, 2, 5];
            MinHeap.sort(A, this.compareFunction);
            prev = A[0];

            expect(A.every(function (curr) {
                var cmp = (prev >= curr);
                prev = curr;
                return cmp;
            })).to.be.true;
        });

});
