var chai = require('chai'), expect = chai.expect;
var TST = require('../lib/TST.js');

describe("TST Suite", function () {
    describe ("put tests: ", function () {
        beforeEach(function () {
            this.TST = new TST();
        });

        it ("should create a new three-node TST",
            function () {
                var TST, node, count, word;

                TST = this.TST;
                TST.put("sea", 0);
                node = TST.root;
                count = 0;
                word = '';

                while(node !== null) {
                    count += 1;
                    word += node.ch;
                    node = node.mid;
                }
                expect(count === 3 && word === 'sea').to.be.true;
            });

        it ("should create a left subtree off the first character of the first word ",
            function () {
                var TST, node, count, word;

                TST = this.TST;
                TST.put("sea", 0);
                TST.put("by", 1);
                node = TST.root;
                node = node.left;
                count = 0;
                word = '';

                while(node !== null) {
                    count += 1;
                    word += node.ch;
                    node = node.mid;
                }

                expect(count === 2 && word === 'by').to.be.true;
            });


        it ("should create a right subtree off the first character of the first word ",
            function () {
                var TST, node, count, word;

                TST = this.TST;
                TST.put("sea", 0);
                TST.put("the", 1);
                node = TST.root;
                node = node.right;
                count = 0;
                word = '';

                while(node !== null) {
                    count += 1;
                    word += node.ch;
                    node = node.mid;
                }

                expect(count === 3 && word === 'the').to.be.true;
            });

        it ("should fork off a left subtree on a character-node of an existing word",
            function () {
                var TST, node, index, word, key;

                key = "shall";
                TST = this.TST;
                TST.put("she", 0);
                TST.put(key, 1);
                node = TST.root;
                word = '';

                for (index=0;; node = node.mid, index++) {
                    if (node.ch === key.charAt(index)) {
                        word += node.ch;
                    }

                    if (node.mid === null) {
                        break;
                    }
                }

                node = node.left;

                while (node !== null) {
                    if (node.ch === key.charAt(index++)) {
                        word += node.ch;
                    }
                    node = node.mid;
                }
                expect(word.length === 5 && word === 'shall').to.be.true;
            });

        it ("should fork off a right subtree on a character-node of an existing word",
            function () {
                var TST, node, index, word, key;

                TST = this.TST;
                TST.put("she", 0);
                key = "shell";
                TST.put(key, 1);
                node = TST.root;
                word = '';

                for (index=0;; node = node.mid, index++) {
                    word += node.ch;
                    if (node.mid === null) {
                        break;
                    }
                }

                node = node.right;

                while (node !== null) {
                    if (node.ch === key.charAt(index++)) {
                        word += node.ch;
                    }
                    node = node.mid;
                }
                expect(word.length === 5 && word === 'shell').to.be.true;
            });
    });

    describe("get tests: ", function () {
        beforeEach(function () {
            this.TST = new TST();
        });

        it("should return the value associated with a key",
            function () {
                var TST;

                TST = this.TST;
                TST.put("somnambulist", 3);
                expect(TST.get("somnambulist")).to.equal(3);
            });

        it("should return null for a key not found in trie",
            function () {
                var TST;

                TST = this.TST;
                TST.put("somnambulist", 3);
                expect(TST.get("sommelier")).to.be.null;
            });

        it("should return null for an empty tree",
            function () {
                var TST;

                TST = this.TST;
                expect(TST.get("onomatopeia")).to.be.null;
            });
    });

    describe("contains tests: ", function() {
        beforeEach(function () {
            var words = [
                'amber',
                'jurassic',
                'she',
                'sells',
                'smile',
                'seclude',
                'seduce',
                'select',
                'senile'];

            var tst = new TST();

            words.forEach(function (word) {
                tst.put(word, word.length);
            });

            this.TST = tst;
        });

        it('should return true if a key is found in trie',
            function() {
                expect(this.TST.contains('smile')).to.be.true;
            });

        it('should return false if a key is not found in trie',
            function() {
                expect(this.TST.contains('amb')).to.be.false;
            });
    });


    describe("_get tests: ", function () {
        beforeEach(function () {
            var words = [
                'amber',
                'jurassic',
                'she',
                'sells',
                'smile',
                'seclude',
                'seduce',
                'select',
                'senile'];

            var tst = new TST();

            words.forEach(function (word) {
                tst.put(word, word.length);
            });

            this.TST = tst;
        });

        it('should find a node that matches prefix',
            function () {
                var tst, node;

                tst = this.TST;
                node = tst._get(tst.root, 'se', 0);
                expect(node.ch === 'e' && node !== null).to.be.true;
            });

        it('should return null for a longer prefix not found in the trie',
            function () {
                var tst = this.TST;

                expect(tst._get(tst.root, 'sea', 0)).to.be.null;
            });

        it('should return null for a prefix not found in the trie',
            function () {
                var tst = this.TST;

                expect(tst._get(tst.root, 'zoo', 0)).to.be.null;
            });

        it('should return null for an empty tree',
            function () {
                var tst = new TST();

                expect(tst._get(tst.root, 'zoo', 0)).to.be.null;
            });
    });

    describe('hasPrefix tests: ', function () {
        beforeEach(function () {
            var words = [
                'amber',
                'jurassic',
                'she',
                'sells',
                'smile',
                'seclude',
                'seduce',
                'select',
                'senile'];

            var tst = new TST();

            words.forEach(function (word) {
                tst.put(word, word.length);
            });

            this.TST = tst;
        });

        it('should return true if prefix exists in trie',
            function() {
                var tst;

                tst = this.TST;
                expect(tst.hasPrefix('se')).to.be.true;
            });

        it('should return false if prefix does not exist in trie',
            function() {
                var tst;

                tst = this.TST;
                expect(tst.hasPrefix('sa')).to.be.false;
            });
    });

    describe('keysWithPrefix tests: ', function () {
        beforeEach(function () {
            var words = [
                'amber',
                'jurassic',
                'she',
                'sells',
                'smile',
                'same',
                'seclude',
                'score',
                'sea',
                'send',
                'heap',
                'state',
                'finite',
                'seduce',
                'tree',
                'depth',
                'breadth',
                'select',
                'seriously',
                'lambda',
                'senile',
                'ambulatory',
                'seize',
                'therapeutic',
                'algorithms',
                'secretive',
                'trie',
                'sensation',
                'swift',
                'secret'
            ];

            var tst = new TST();

            words.forEach(function (word) {
                tst.put(word, word.length);
            });

            this.words = words;
            this.TST = tst;
        });

        it('should return keys that match prefix',
            function () {
                var tst, seExpectedKeys, seActualKeys;

                tst= this.TST;
                seExpectedKeys = [
                 'sells',
                 'seclude',
                 'sea',
                 'send',
                 'seduce',
                 'select',
                 'seriously',
                 'senile',
                 'seize',
                 'secretive',
                 'sensation',
                 'secret'
                ].sort();

                seActualKeys = tst.keysWithPrefix('se');
                expect(seExpectedKeys.every(
                    function(key) {
                        return seActualKeys.indexOf(key) > -1;
                    })).to.be.true;
            });

        it('should return an empty array if prefix does not appear in the trie',
            function () {
                var tst, seExpectedKeys, seActualKeys;

                tst= this.TST;
                seActualKeys = tst.keysWithPrefix('set');
                expect(seActualKeys.length).to.equal(0);
            });

        it('should return an empty array if trie is empty',
            function () {
                var tst;

                tst = new TST();
                expect(tst.keysWithPrefix('ab').length).to.equal(0);
            });

    });

    describe('keysThatMatch tests: ', function () {
        beforeEach(function() {
            var tst, words;

            words = [
                    'keeper',
                    'gape',
                    'gear',
                    'greater',
                    'flower',
                    'fear',
                    'bleak',
                    'beak',
                    'claw',
                    'cease',
                    'dirge',
                    'deride',
                    'divulge',
                    'divide',
                    'dear',
                    'dearth',
                    'mantra',
                    'nadir',
                    'near',
                    'over',
                    'rapport',
                    'rear',
                    'stencil',
                    'samba',
                    'sea',
                    'seize',
                    'umbrella',
                    'viola',
                    'transit',
                    'tear',
                    'tea',
                    'swift',
                    'secret'
            ];

            tst = new TST();

            words.forEach(function (word) {
                tst.put(word, word.length);
            });

            this.TST = tst;
        });

        it('should return keys that match wildcard-prefix',
            function () {
                var tst, keys, expectedKeys;

                tst = this.TST;
                expectedKeys = [
                    'gear',
                    'fear',
                    'beak',
                    'cease',
                    'dear',
                    'dearth',
                    'near',
                    'rear',
                    'sea',
                    'tear',
                    'tea'
                ].sort();

                keys = tst.keysThatMatch('.ea');
                expect(expectedKeys).deep.equal(keys);
        });

        it('should return keys that match wildcard-suffix',
            function () {
                var tst, keys, expectedKeys;

                tst = this.TST;

                expectedKeys = [
                    'stencil',
                    'samba',
                    'sea',
                    'seize',
                    'swift',
                    'secret'
                ].sort();

                keys = tst.keysThatMatch('s..');
                expect(expectedKeys).deep.equal(keys);
            });

        it('should return keys that match pattern with non-prefix and non-suffix wildcard characters',
            function () {
                var tst, keys, expectedKeys;

                tst = this.TST;

                expectedKeys = ['deride', 'divide'];
                keys = tst.keysThatMatch('d..i');
                expect(expectedKeys).deep.equal(keys);
            });


        it('should return an empty array when there no keys that match the wildcard pattern',
            function () {
                var tst, keys, expectedKeys;

                tst = this.TST;

                keys = tst.keysThatMatch('s.t');
                expect(keys).to.be.empty;
            });

        it('should return an empty array when TST is empty',
            function () {
                var tst, keys, expectedKeys;

                tst = new TST();

                keys = tst.keysThatMatch('ab.');
                expect(keys).to.be.empty;
            });

    });

    describe("keys tests: ", function () {
        it('should return all of the keys in the TST',
            function () {
                var tst, words;

                words = [
                        'keeper',
                        'gape',
                        'gear',
                        'greater',
                        'flower',
                        'fear',
                        'bleak',
                        'beak',
                        'claw',
                        'cease',
                        'dirge',
                        'deride',
                        'divulge',
                        'divide',
                        'dear',
                        'dearth',
                        'mantra',
                        'nadir',
                        'near',
                        'over',
                        'rapport',
                        'rear',
                        'stencil',
                        'samba',
                        'sea',
                        'seize',
                        'umbrella',
                        'viola',
                        'transit',
                        'tear',
                        'tea',
                        'swift',
                        'secret'
                ];

                tst = new TST();

                words.forEach(function (word) {
                    tst.put(word, word.length);
                });

                expect(tst.keys()).to.deep.equal(words.sort());
            });

        it('should return an empty array when retrieving keys from an empty TST',
            function () {
                var tst;

                tst = new TST();
                expect(tst.keys()).to.be.empty;

            });
    });

    describe('longestPrefixOf tests: ', function () {
        beforeEach(function() {
            var words, tst;

            words = "sea shells she shell sort quick heap algorithm banana axiom axiomatic";
            tst = new TST();

            words.split(" ").forEach(function (word) {
                tst.put(word, word.length);
            });

            this.TST = tst;
        });

        it('should return the longest prefix of a key that contains a prefix-key',
            function () {
                var tst = this.TST;

                expect(tst.longestPrefixOf("axiomatic")).to.equal("axiom");
            });

        it('should return an empty string if the key has no prefix-key',
            function () {
                var tst = this.TST;

                expect(tst.longestPrefixOf("algorithm")).to.equal("");
            });

        it('should return an empty string if the trie is empty',
            function () {
                var tst = new TST();

                expect(tst.longestPrefixOf("wassup")).to.equal("");
            });

    });
});
