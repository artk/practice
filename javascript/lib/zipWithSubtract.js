/**
 * Created by art on 5/27/14.
 */

var zipWith = require('./zipWith.js');

var zipWithSubtract = zipWith(function (x, y) {
    return x - y;
});

module.exports = zipWithSubtract;