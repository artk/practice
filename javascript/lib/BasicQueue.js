/**
 * Created by art on 6/2/14.
 */

function BasicQueue() {
    var self = this instanceof BasicQueue ? this :
        Object.create(BasicQueue.prototype);

    self.queue = [];
    return self;
}

BasicQueue.prototype.constructor = BasicQueue;

BasicQueue.prototype.add = function (o) {
    this.queue.push(o);
};


BasicQueue.prototype.remove = function () {
    if (this.isEmpty()) throw Error("Queue is empty.");

    return this.queue.shift();
};


BasicQueue.prototype.isEmpty = function () {
    return this.queue.length === 0;
};


BasicQueue.prototype.size = function () {
    return this.queue.length;
};

module.exports = BasicQueue;
