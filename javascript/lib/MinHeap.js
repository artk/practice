var Heap = require('./Heap.js');
var inequality = require('./inequality');

/**
 * Returns a MinHeap object.
 * The compareFunction takes two parameters, a and b. It returns a negative
 * number if a < b, zero if a === b and a positive number if a > b
 *
 * @param compareFunction a comparison function that returns a number
 * @constructor
 */
function MinHeap(compareFunction, list) {
    "use strict";
    var self;

    if (compareFunction === undefined) {
        throw new Error("compareFunction function is missing.");
    }

    if (typeof compareFunction !== 'function') {
        throw new Error("compareFunction must be a function");
    }

    self = this instanceof MinHeap ? this : Object.create(MinHeap.prototype);
    self.less = inequality.less(compareFunction);
    Heap.call(self, list);
    return self;
}

MinHeap.prototype = Object.create(Heap.prototype);
MinHeap.prototype.constructor = MinHeap;

MinHeap.sort = function (A, compareFunction) {
    Heap.sort(A, inequality.less(compareFunction));
};

MinHeap.prototype.swim = function (k) {
    "use strict";
    Heap.swim(this.heap, k, this.less);
};

MinHeap.prototype.sink = function (k) {
    "use strict";
    Heap.heapify(this.heap, k, this.heap.length - 1, this.less);
};

module.exports = MinHeap;
