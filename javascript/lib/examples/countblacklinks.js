// test program
var LLRBTree = require ('../LLRBTree.js');
var llrb = new LLRBTree(function (a, b) { return a-b; });

var isLLRB = function (llrb) {
    var isLLRB;

    function pathLength(node, count, blackLinks) {
        if (node === null) {
            if (!blackLinks.count) {
                blackLinks.count = count;
            }
            else if (blackLinks.count !== count) {
                throw new Error("Black Links break invariant [" + blackLinks.count + ", " + count +"]");
            }

            return;
        }

        if (!node.isRed()) {
            count++;
        }

        pathLength(node.left, count, blackLinks);
        pathLength(node.right, count, blackLinks);
    }


    try {
        pathLength(llrb.root, 0, {});
        isLLRB = true;
    } catch (err) {
        isLLRB = false;
    }

    return isLLRB;
};




var badblacklist= [
  239,
  105,
  33,
  352,
  198,
  238,
  191,
  56,
  294,
  160,
  153,
  109,
  423,
  241,
  147,
  401,
  327,
  372,
  396,
  83,
  125,
  9,
  407,
  72,
  337,
  28,
  24,
  47,
  315,
  377 ];

var dumpTree = function dump(node, tab) {

    if (node === null) {
        return;
    }

    if (tab === undefined) tab = 0;

    for (var i=0, str=""; i < tab; i++) {
        str += '.';
    }
    str += node.key + " " + (node.isRed() ? "R" : "B") +  "[" +
        (node.left? node.left.key : "null")+ ", " +
        (node.right? node.right.key : "null") + "]";

    console.log(str);
    dump(node.left, tab+1);
    dump(node.right, tab+1);
};


badblacklist.forEach(function (item) {
    llrb.insert(item);
    dumpTree(llrb.root);
    console.log();
    if (!isLLRB(llrb)) {
        throw new Error("black links invariant broken after inserting " + item);
    }
});

/*
llrb.insert(239);
llrb.insert(105);
llrb.insert(33);
llrb.insert(352);
llrb.insert(198);
llrb.insert(238);
dumpTree(llrb.root);
llrb.insert(191);
*/

console.log();
dumpTree(llrb.root);
/*
var numbers = Math.ceil(Math.random() * 3) * 10;
var inputs = [];
for (var i=0; i < numbers; i++) {
    var t = Math.ceil(Math.random() * 500);
    llrb.insert(t);
    inputs.push(t);
}

console.log(inputs);
*/
