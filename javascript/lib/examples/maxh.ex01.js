var MaxHeap = require('../MaxHeap.js');

var mh = new MaxHeap(function (a, b) {
    return a - b;
});

var inputs = [17, 5, 9, 4, 23, 67, 51, 19, 2, 45, 101, 12, 22, 41];

inputs.forEach(function (item) {
    mh.insert(item);
});

console.log(mh.heap);
while (!mh.isEmpty()) {
    console.log(mh.extract());
}
console.log(mh.heap);
