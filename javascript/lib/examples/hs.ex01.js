var heapsort = require('../heapsort.js');
var A = [680, 17, 101, 31415, 2718, 42, 622, 39, 94, 280, 880, 355, 294, 42];
var cmpFn = function (a, b) { return a - b; };

console.log("Source: ", A);
console.log();
heapsort(A, cmpFn);
console.log("Ascending Order: ", A);
heapsort(A, cmpFn, true);
console.log("Descending Order: ", A);
