var MathExtras = {
    isPerfectSquare: function (num) {
        var sqrt = Math.sqrt(num);
        var floor = Math.floor(sqrt);
        return Math.pow(sqrt, 2) === Math.pow(floor, 2);
    },

    /**
     *
     * Reference: http://www.cs.utexas.edu/users/djimenez/utsa/cs3343/lecture25.html
     *
     * N > 15 slows down this method
     *
     * returns an array of combinations
     * @param N
     * @param k
     */

    combinations: function (N, k) {
        var i, c, j, comboSet, combo;

        comboSet = [];
        for (i = 0; i < (1 << N); i++) {
            combo = [];
            for (j = 0, c = 0; j < 32; j++) {
                if (i & (1 << j)) {
                    ++c;
                    combo.push(j + 1);
                }
            }

            if (c === k) {
                comboSet.push(combo);
            }
        }
        return comboSet;
    },

    combinations1: function (N, k) {

        var comboSet = [], v = [];

        v.length = k;

        function combinations(comboSet, v, start, n, k, maxk) {
            var i;

            /* k here counts through positions in the maxk-element v.
             * if k > maxk, then the v is complete and we can use it.
             */
            if (k > maxk) {
                /* insert code here to use combinations as you please */
                comboSet.push(v.slice());
                return;
            }

            /* for this k'th element of the v, try all start..n
             * elements in that position
             */
            for (i = start; i <= n; i++) {

                v[k - 1] = i;

                /* recursively generate combinations of integers
                 * from i+1..n
                 */
                combinations(comboSet, v, i + 1, n, k + 1, maxk);
            }
        }

        combinations(comboSet, v, 1, N, 1, k);

        return comboSet;
    }

};


module.exports = MathExtras;
