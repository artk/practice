var MinHeap = require('./MinHeap.js');

function MinPQ(compareFunction) {
    "use strict";
    var self = this instanceof MinPQ ? this : Object.create(MinPQ.prototype),
        mh = new MinHeap(compareFunction);

    self.add = function (elem) {
        mh.insert(elem);
    };

    self.remove = function () {
        return mh.extract();
    };

    self.isEmpty = function () {
        return mh.isEmpty();
    };
    return self;
}

module.exports = MinPQ;
