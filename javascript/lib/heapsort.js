var MinHeap = require('./MinHeap.js');
var MaxHeap = require('./MaxHeap.js');

function heapsort(A, compareFunction, desc) {
    if (desc) {
        MinHeap.sort(A, compareFunction);
    }
    else {
        MaxHeap.sort(A, compareFunction);
    }
}

module.exports = heapsort;
