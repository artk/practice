/**
 * Created by art on 5/27/14.
 */

zipWith = function (f) {
    return function (A, B) {
        if (!Array.isArray(A) || !Array.isArray(B))
            throw Error();

        var size = A.length < B.length ? A.length : B.length;

        var res = [];
        res.length = size;
        for (var i = 0; i < size; i++)
            res[i] = f(A[i], B[i]);

        return res;
    }
};

module.exports = zipWith;