function BitMap(numBits) {
    "use strict";
    var INTSIZE = 32,
        bitmap = [],
        mapSize = indexOf(numBits) + (pos(numBits) > 0 ? 1 : 0);

    function clear() {
        for (var i = 0; i < mapSize; i++) {
            bitmap[i] = 0;
        }
    };

    /**
     * returns the bit's index in the bitmap
     * @param bit
     * @returns {number}
     */
    function indexOf(bit) {
        return Math.floor(bit / INTSIZE);
    };

    /**
     * returns the bit position of the Integer
     * @param bit
     * @returns {number}
     */
    function pos(bit) {
        return bit % INTSIZE;
    };

    /**
     * return a validated index of the bit in the bitmap
     * @param bit
     * @returns {number}
     */
    function inBoundsIndexOf(bit) {
        var i = indexOf(bit);

        if (i < 0 || i >= bitmap.length) {
            throw new Error("no such bit");
        }

        return i;
    }

    var self = (this instanceof BitMap) ? this
        : Object.create(BitMap.prototype);


    self.set = function (bit) {
        var i = inBoundsIndexOf(bit);
        bitmap[i] |= (1 << pos(bit));
    };


    self.unset = function (bit) {
        var i = inBoundsIndexOf(bit);

        bitmap[i] &= ~(1 << pos(bit));
    };

    self.isSet = function (bit) {
        var i = inBoundsIndexOf(bit);
        return (bitmap[i] & (1 << pos(bit))) !== 0;
    }

    self.clear = function () {
        clear();
    };

    clear();  // init bitmap
    return self;
};


module.exports = BitMap;
