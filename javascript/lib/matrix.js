var matrix = function (ROWS, COLS) {

    var arr = [];
    arr.length = ROWS;

    for (var i = 0; i < ROWS; i++) {
        arr[i] = [];
        arr[i].length = COLS;
    }

    return arr;
};

module.exports = matrix;
