var binnode = require('./binnode.js');
/**
 * Binary Search Tree constructor function
 * @param compareFunction
 * @returns {BST}
 * @constructor
 */
function BST(compareFunction) {
    "use strict";
    var self;

    if (compareFunction === 'undefined') {
        throw new Error("compareFunction function is missing.");
    }

    if (typeof compareFunction !== 'function') {
        throw new Error("compareFunction must be a function");
    }

    self = this instanceof BST ? this : Object.create(BST.prototype);
    self.compare = compareFunction;
    self.root = null;
    return self;
}


BST.prototype.parent = function (node) {
    "use strict";
    var self = this;

    // every node is its own ancestor (parent)
    function parent(key, node, p) {
        var cmp;

        if (node === null) {
            return null;
        }

        cmp = self.compare(key, node.key);

        if (cmp === 0) {
            return p;
        }
        if (cmp < 0) {
            return parent(key, node.left, node);
        }
        return parent(key, node.right, node);
    }

    if (node === null) {
        return null;
    }
    return parent(node.key, this.root, this.root);
};


// inorder predecessor
BST.prototype.predecessor = function (node) {
    "use strict";
    var x, y;

    if (node === null) {
        return null;
    }

    if (node.left !== null) {
        return this.max(node.left);
    }

    if (node === this.min(this.root)) {
        return null;
    }

    x = node;
    y = this.parent(x);

    while (y !== null && x === y.left) {
        x = y;
        y = this.parent(y);
    }

    return y;
};


// inorder successor
BST.prototype.successor = function (node) {
    "use strict";
    var x, y;
    if (node === null) {
        return null;
    }
    else if (node.right !== null) return this.min(node.right);
    else if (node === this.max(this.root)) return null;

    x = node;
    y = this.parent(x);

    while (y !== null && x === y.right) {
        x = y;
        y = this.parent(y);
    }

    return y;
};


BST.prototype.insert = function (key, value) {
    "use strict";
    var self = this;

    function insert(node, key, value) {
        var cmp;

        if (node === null) return binnode(key, value);

        cmp = self.compare(key, node.key);

        if (cmp < 0) node.left = insert(node.left, key, value);
        else if (cmp > 0) node.right = insert(node.right, key, value);
        else node.value = value;  // duplicates not allowed (for now)

        return node;
    };

    if (key === void 0) throw Error("No key provided.");
    if (value === void 0) value = key;  // use key as value

    this.root = insert(this.root, key, value);
};


BST.prototype.delete = function (key) {
    "use strict";
    var self = this, node;

    function remove(node, key) {
        var cmp;

        if (node === null) return null;

        cmp = self.compare(key, node.key);

        if (cmp < 0) {
            node.left = remove(node.left, key);
        } else if (cmp > 0) {
            node.right = remove(node.right, key);
        } else {
            if (node.right === null) {
                return node.left;
            } else if (node.left === null) {
                return node.right;
            } else {
                var t = node;
                node = self.min(t.right);
                node.right = self.deleteMin(t.right);
                node.left = t.left;
            }
        }

        return node;
    }

    this.root = remove(this.root, key);
};


BST.prototype.deleteMin = function (node) {
    "use strict";
    function removeMin(node) {
        if (node.left === null) return node.right;

        node.left = removeMin(node.left);
        return node;
    }

    if (node === undefined) {
        this.root = removeMin(this.root);
    } else {
        return removeMin(node);
    }
};


BST.prototype.deleteMax = function (node) {

    function removeMax(node) {
        if (node.right === null) return node.left;

        node.right = removeMax(node.right);
        return node;
    }

    if (node === undefined) this.root = removeMax(this.root);
    else return removeMax(node);
};


BST.prototype.search = function (key) {
    var self = this;

    function find(node, key) {
        var cmp;

        if (node === null) return null;

        cmp = self.compare(key, node.key);

        if (cmp === 0) return node;
        else if (cmp < 0) return find(node.left, key);
        else return find(node.right, key);

    }

    if (this.isEmpty()) return null;
    else return find(this.root, key);
};


BST.prototype.min = function (node) {

    function min(node) {
        if (node.left === null) return node;
        else return min(node.left);
    }

    if (node === undefined) return min(this.root);
    else return min(node)
};


BST.prototype.max = function (node) {

    function max(node) {
        if (node.right === null) return node;

        return max(node.right);
    }

    if (node === void 0) return max(this.root);
    else return max(node);
};


BST.prototype.isEmpty = function () {
    return this.root === null;
};

BST.prototype.keys = function () {
    return this.inorder();
}

BST.prototype.inorder = function () {
    var order = [];

    function inorder(T) {
        if (T == null) return;

        inorder(T.left);
        order.push(T.key);
        inorder(T.right);
    }

    if (this.root === null) return "";

    inorder(this.root);
    return order;
};


BST.prototype.preorder = function () {
    var order = [];

    function preorder(T) {
        if (T == null) return;

        order.push(T.key);
        preorder(T.left);
        preorder(T.right);
    }

    if (this.root) {
        preorder(this.root);
    }

    return order;
};

BST.prototype.postorder = function () {
    "use strict";
    var order = [];

    function postorder(T) {

        if (T === null) {
            return;
        }

        postorder(T.left);
        postorder(T.right);
        order.push(T.key);
    }

    if (this.root) {
        postorder(this.root);
    }
    return order;
};


module.exports = BST;
