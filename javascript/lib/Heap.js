/**
 * Returns a Heap object.
 *
 * @param compareFunction function(a,b) that returns a positive number if a>b, 0 if a equals b and negative number if a < b
 * @returns {Heap}
 * @constructor
 */


function Heap(list) {
    "use strict";
    var self, i;

    if (list !== undefined && !Array.isArray(list)) {
        throw new Error("list must be an Array");
    }

    self = this instanceof Heap ? this : Object.create(Heap.prototype);

    if (list === undefined) {
        self.heap = [];
    } else {
        self.heap = list;
        for (i = Math.floor((self.heap.length - 1) / 2); i >= 0; i--) {
            this.sink(i);
        }
    }
    return self;
}

Heap.exchange = function (arr, i, j) {
    "use strict";
    var tmp;

    tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
};

Heap.LOWERBOUND = 0;

/**
 * Returns the parent of node at index i
 *
 * @param i
 * @returns {number}
 */
Heap.parent = function (i) {
    "use strict";
    return Math.floor((i - 1) / 2);
};

/**
 * Returns the left child of the node at index i
 *
 * @param i
 * @returns {number}
 */
Heap.left = function (i) {
    "use strict";
    return i * 2 + 1;
};

/**
 * Returns the right child of the node at index i
 *
 * @param i
 * @returns {number}
 */
Heap.right = function (i) {
    "use strict";
    return i * 2 + 2;
};

/**
 *
 * @param A an array
 * @param k index of tree to be heapified
 * @param last last element of A
 * @param inequality a boolean function that returns true if two values are different
 */
Heap.heapify = function (A, k, last, inequality) {
    "use strict";
    var left, right, child;
    for (left = Heap.left(k); left <= last; left = Heap.left(k)) {
        right = Heap.right(k);
        if (!(right > last || inequality(A[left], A[right]))) {
            child = right;
        } else {
            child = left;
        }

        if (inequality(A[k], A[child])) {
            break;
        }

        Heap.exchange(A, k, child);
        k = child;
    }
};

Heap.makeHeap = function (A, inequality) {
    var k, last;

    if (!Array.isArray(A) || typeof inequality !== 'function') {
        return;
    }

    for (last = A.length-1, k = Math.floor((last - 1) / 2); k >=0; k--) {
        Heap.heapify(A, k, last, inequality);
    }
};

Heap.sort = function (A, inequality) {
    var k, last;
    if (!Array.isArray(A) || typeof inequality !== 'function') {
        return;
    }

    Heap.makeHeap(A, inequality);

    for (k=0, last=A.length-1; k !== last; last--) {
        Heap.exchange(A, k, last);
        Heap.heapify(A, 0, last-1, inequality);
    }
}

/**
 *
 * @param A
 * @param k
 * @param inequality a boolean function that returns true if two values are different
 */
Heap.swim = function (A, k, inequality) {
    "use strict";
    var parent;

    while (k > Heap.LOWERBOUND) {
        parent = Heap.parent(k);

        if (inequality(A[parent], A[k])) {
            break;
        }

        Heap.exchange(A, parent, k);
        k = parent;
    }
};

Heap.prototype.isEmpty = function () {
    "use strict";
    return this.heap.length === 0;
};

Heap.prototype.size = function () {
    "use strict";
    return this.heap.length;
};

Heap.prototype.insert = function (elem) {
    "use strict";
    this.heap.push(elem);
    this.swim(this.heap.length - 1);  // swim implemented by child object
};


/**
 * Returns the min item for MinHeap and max item for MaxHeap
 * @returns {*}
 */
Heap.prototype.peek = function () {
    "use strict";
    var heap = this.heap;

    if (this.isEmpty()) {
        throw new Error("heap is empty");
    }

    return heap[Heap.LOWERBOUND];
};

/**
 * This is the equivalent of extractMax when called by a MaxHeap object
 * and the equivalent of extractMin when called by a MinHeap object.
 * @returns {*}
 */
Heap.prototype.extract = function () {
    "use strict";

    var key, last, heap = this.heap;

    if (this.isEmpty()) {
        throw new Error("Heap is empty.");
    }

    key = heap[Heap.LOWERBOUND];
    last = heap.pop();

    if (heap.length > 0) {
        heap[Heap.LOWERBOUND] = last;
        this.sink(Heap.LOWERBOUND);
    }
    return key;
};

module.exports = Heap;


