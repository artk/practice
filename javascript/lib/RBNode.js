function RBNode(key, value, black) {
    "use strict";
    var rb, self;

    self = this instanceof RBNode ? this : Object.create(RBNode.prototype);

    if (black !== undefined) {
        self.color = false; // Black
    } else {
        self.color = true;  // Red
    }

    self.key = key;
    self.value = value;
    self.left = null;
    self.right = null;
    return self;
}


RBNode.prototype.isRed = function () {
    return this.color === true;
};

RBNode.prototype.setBlack = function () {
    this.color = false;
};

RBNode.prototype.setRed = function () {
    this.color = true;
};

module.exports = RBNode;
