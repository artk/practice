/**
 * Created by art on 5/27/14.
 */

var zipWith = require('./zipWith.js');

var zipWithAdd = zipWith(function (x, y) {
    return x + y;
});

module.exports = zipWithAdd;