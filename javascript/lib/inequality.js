var inequality = {
    greater : function (compareFunction) {
        return function(a, b) {
            return compareFunction(a, b) > 0;
        };
    },

    less : function (compareFunction) {
        return function (a, b) {
            return compareFunction(a, b) < 0;
        };
    }
};

module.exports = inequality;
