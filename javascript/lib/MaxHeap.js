var Heap = require('./Heap.js');
var inequality = require('./inequality.js');

/**
 * Returns a MaxHeap object.
 * The compareFunction takes two parameters, a and b. It returns a negative
 * number if a < b, zero if a === b and a positive number if a > b
 *
 * @param compareFunction a comparison function that returns a number
 * @constructor
 */
function MaxHeap(compareFunction, list) {
    "use strict";
    var self;

    if (compareFunction === undefined) {
        throw new Error("compareFunction function is missing.");
    }

    if (typeof compareFunction !== 'function') {
        throw new Error("compareFunction must be a function");
    }

    self = this instanceof MaxHeap ? this : Object.create(MaxHeap.prototype);
    self.greater = inequality.greater(compareFunction);
    Heap.call(self, list);
    return self;
}

MaxHeap.prototype = Object.create(Heap.prototype);
MaxHeap.prototype.constructor = MaxHeap;

MaxHeap.sort = function (A, compareFunction) {
    Heap.sort(A, inequality.greater(compareFunction));
};

MaxHeap.prototype.swim = function (k) {
    "use strict";
    Heap.swim(this.heap, k, this.greater);
};

MaxHeap.prototype.sink = function (k) {
    "use strict";
    Heap.heapify(this.heap, k, this.heap.length - 1, this.greater);
};


module.exports = MaxHeap;
