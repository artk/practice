var binnode = function (key, value) {
    "use strict";
    return {
        key: key,
        value: value,
        left: null,
        right: null
    };
};

module.exports = binnode;
