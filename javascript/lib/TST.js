/**
 * Ternary Search Trie
 *
 */
function TST() {
    var self;

    self = this instanceof TST ? this : Object.create(TST.prototype);
    self.root = null;
    return self;
}

TST.prototype.tstNode = function (ch) {
    return {
        ch : ch,
        value : null,
        left : null,
        mid : null,
        right : null
    };
};


TST.prototype.put = function (key, value) {
    var self;

    function put(node, key, value, depth) {
        if (node === null) {
            node = self.tstNode();
            node.ch = key.charAt(depth);
        }

        if (key.charAt(depth) < node.ch) {
            node.left = put(node.left, key, value, depth);
        }
        else if (key.charAt(depth) > node.ch) {
            node.right = put(node.right, key, value, depth);
        }
        else if (depth === (key.length - 1)) {
            node.value = value;
        }
        else {
            node.mid = put(node.mid, key, value, depth + 1);
        }

        return node;
    }

    self = this;
    this.root = put(this.root, key, value, 0);
};


// "private" method
TST.prototype._get = function get(node, s, depth) {
    var c;

    if (node === null) {
        return null;
    }

    c = s.charAt(depth);

    if (c < node.ch) {
        return get(node.left, s , depth);
    }
    else if (c > node.ch) {
        return get(node.right, s, depth);
    }
    else if (depth === (s.length - 1)) {
        return node;
    }
    else {
        return get(node.mid, s, depth + 1);
    }
};


TST.prototype.get = function (key) {
    var node;

    node = this._get(this.root, key, 0);

    if (node !== null) {
        return node.value;
    }

    return node;
};


TST.prototype.delete = function (key) {
};


TST.prototype.contains = function (key) {
    return this.get(key) !== null;
};


TST.prototype.isEmpty = function () {
};


TST.prototype.longestPrefixOf = function (s) {
// the longest key that is a prefix of a given string,
    var len;

    function _longestPrefixOf(node, key, depth, prefLen) {
        var ch;

        if (node === null) {
            return 0;
        }

        ch = key.charAt(depth);

        if (ch < node.ch) {
            return _longestPrefixOf(node.left, key, depth, prefLen);
        }
        else if (ch > node.ch) {
            return _longestPrefixOf(node.right, key, depth, prefLen);
        }
        else if (depth < (key.length - 1) ) {
            if (node.value !== null) {
                prefLen = node.value;
            }

            return _longestPrefixOf(node.mid, key, depth+1, prefLen);
        }
        else {
            return prefLen;
        }
    };

    len = _longestPrefixOf(this.root, s, 0, 0);
    return s.substring(0, len)
};


TST.prototype._collect =  function (node, prefStr, keys) {

    function collect(node, prefStr, keys) {
        var newPrefix;

        if (node === null) {
            return;
        }

        collect(node.left, prefStr, keys);
        newPrefix = prefStr + node.ch;

        if (node.value !== null) {
           keys.push(newPrefix);
        }

        collect(node.mid, newPrefix, keys);
        collect(node.right, prefStr, keys);
    }

    if (node === null) {
        return null;
    }

    if (node.value !== null) {
        keys.push(prefStr);
    }

    return collect(node.mid, prefStr, keys);
};


TST.prototype.keysWithPrefix = function (s) {
    var keys;

    keys = [];
    this._collect(this._get(this.root, s, 0), s, keys);
    return keys;
};


TST.prototype.hasPrefix = function (s) {
    return this._get(this.root, s, 0) !== null;
};


TST.prototype.keysThatMatch = function (pattern) {
    // . in pattern represents wildcard character

    var keys, self;

    function _keysThatMatch(node, prefStr, pattern, depth, keys) {
        var chPat;

        if (node === null) {
            return;
        }

        chPat = pattern.charAt(depth);

        if (chPat === '.') {
            _keysThatMatch(node.left, prefStr, pattern, depth, keys);

            if (depth === pattern.length - 1) {
                self._collect(node, prefStr + node.ch, keys);
            }
            else {
                _keysThatMatch(node.mid, prefStr + node.ch, pattern, depth+1, keys);
            }

            _keysThatMatch(node.right, prefStr, pattern, depth, keys);
        }
        else if (chPat < node.ch) {
            _keysThatMatch(node.left, prefStr, pattern, depth, keys);
        }
        else if (chPat > node.ch) {
            _keysThatMatch(node.right, prefStr, pattern, depth, keys);
        }
        else if (depth === pattern.length - 1) {
            self._collect(node, prefStr + node.ch, keys);
        }
        else {
            _keysThatMatch(node.mid, prefStr + node.ch, pattern, depth + 1, keys);
        }
    }

    keys = [];
    self = this;
    _keysThatMatch(this.root, '', pattern, 0, keys);
    return keys;
};

TST.prototype.keys = function () {
    var keys, self;

    function getKeys(node, keys) {

        if (node === null) {
            return;
        }

        getKeys(node.left, keys);
        self._collect(node, node.ch, keys);
        getKeys(node.right, keys);
    }

    keys = [];
    self = this;
    getKeys(this.root, keys);
    return keys;
};


TST.prototype.size = function () {
};


module.exports = TST;
