function Graph() {
    var self, G, V, E;

    self = this instanceof Graph ? this : Object.create(Graph.prototype);
    G = {};
    V = 0; // number of vertices
    E = 0; // number of edges


    function addHelper(v, w) {
        if (!(v in G)) {
            G[v] = [w];
            ++V;
        }
        else if (G[v].indexOf(w) > -1) {
            return;
        }
        else {
            G[v].push(w);
        }
        ++E;
    };


    self.addEdge = function (v, w) {
        if (v === w) {
            throw new Error("v cannot equal w");
        }

        addHelper(v, w);
        addHelper(w, v);

    };

    self.adj = function (v) {
        if (v in G) {
            return [].concat(G[v]);
        }
        else
            return [];
    };

    self.V = function () {
        return V;
    };

    self.E = function () {
        return E;
    };

    return self;
};

module.exports = Graph;
