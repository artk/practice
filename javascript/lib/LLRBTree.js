/*
 *  Robert Sedgewick's Left-Leaning Red Black Trees
 *  https://www.cs.princeton.edu/~rs/talks/LLRB/LLRB.pdf
 *
 *  From Sedgewick's notes, LLRB tree is a BST such that:
 *
 *  1. No node has two red links connected to it.
 *  2. Every path from root to null link has the same number of black links.
 *  3. Red links lean left.
 *  4. Never two red links in a row
 *
 *  Left-leaning Red Black Tree considered harmful? http://www.read.seas.harvard.edu/~kohler/notes/llrb.html
 *
 */

var BST = require('./BST.js');
var RBNode = require('./RBNode.js');


// initial object layout
function LLRBTree(compareFunction) {
    "use strict";
    var self;

    self = this instanceof LLRBTree ? this : Object.create(LLRBTree.prototype);
    BST.call(self, compareFunction);

    return self;
}


LLRBTree.prototype = Object.create(BST.prototype);
LLRBTree.prototype.constructor = LLRBTree;


LLRBTree.prototype.flipColor = function (node) {
    node.right.setBlack();
    node.left.setBlack();
    node.setRed();
}

LLRBTree.prototype.rotateLeft = function (node) {
    var rightChild;

    rightChild = node.right;
    node.right = rightChild.left;
    rightChild.left = node;

    rightChild.color = node.color;
    node.setRed();
    return rightChild;
};


LLRBTree.prototype.rotateRight = function (node) {
    "use strict";
    var leftChild;

    leftChild = node.left;
    node.left = leftChild.right;
    leftChild.right = node;

    leftChild.color = node.color;
    node.setRed();
    return leftChild;
};


LLRBTree.prototype.insert = function (key, value) {
    var self;

    function insert(node, key, value) {
        if (node === null) {
            return new RBNode(key, value);  // RBNode's default color is red
        }

        if (self.compare(key, node.key) < 0) {
            node.left = insert(node.left, key, value);
        }
        else if (self.compare(key, node.key) > 0) {
            node.right = insert(node.right, key, value);
        }
        else {
            node.value = value;
        }

        if (node.right && node.right.isRed() && (!node.left || !node.left.isRed())) {
            node = self.rotateLeft(node);
        }
        if (node.left && node.left.left && node.left.isRed() && node.left.left.isRed()) {
            node = self.rotateRight(node);
        }

        if (node.left  && node.left.isRed() && node.right && node.right.isRed()) {
            self.flipColor(node);
        }
        return node;
    }

    self = this;
    this.root = insert(this.root, key, value);
    this.root.setBlack();
};


LLRBTree.prototype.remove = function (key) {
};


module.exports = LLRBTree;
