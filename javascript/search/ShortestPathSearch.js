function ShortestPathSearch(queue) {
    var self;

    if (queue === undefined) {
        throw new Error("queue not supplied");
    }

    self = this instanceof ShortestPathSearch ? this : Object.create(ShortestPath.prototype);
    self.queue= queue;

    return self;
}

ShortestPathSearch.prototype.execute = function () {
    /*
        path - an array of states and actions;
              [state, action, state, ..., action, state]
        successor - method that takes a state as an argument and returns a dictionary
                    of state/action (key/value) pairs.
        isGoal - method that takes a state as argument and returns a boolean

        successor and isGoal are implemented by child objects

     */
    var frontier, explored, path, path2, stateActionSet, state, action;

    if (this.isGoal(this.start)) {
        return [this.start];
    }

    frontier = this.queue;
    frontier.add([this.start]);
    explored = {};

    while (!frontier.isEmpty()) {
        path = frontier.remove();
        stateActionSet= this.successor(this.pathFinalState(path));

        for (state in stateActionSet) {
            if (Object.prototype.hasOwnProperty.call(stateActionSet, state)) {
                action = stateActionSet[state];
                if (! (state in explored)) {
                    explored[state] = true;
                    path2 = path.slice();
                    path2.push(action);
                    path2.push(state);

                    if (this.isGoal(state)) {
                        return path2;
                    }
                    else  {
                        frontier.add(path2);
                    }
                }

            }
        }
    };
    return [];
};

ShortestPathSearch.prototype.pathFinalState = function (path) {
    return path[path.length-1];
};

module.exports = ShortestPathSearch;
