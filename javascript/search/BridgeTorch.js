var LeastCostSearch = require('./LeastCostSearch.js');
var MathExtras = require('../lib/MathExtras.js');
var MinPQ = require('../lib/MinPQ.js');

/* http://en.wikipedia.org/wiki/Bridge_and_torch_problem */
function BridgeTorch(t1, t2, t3, t4) {
    "use strict";
    var self, people, queue, all, none;

    self = (this instanceof BridgeTorch) ? this : Object.create(BridgeTorch.prototype);
    queue = new MinPQ(self.pathCompare.bind(self));
    LeastCostSearch.call(self, queue);

    people = [t1, t2, t3, t4];
    people.sort(self.sortCompare);
    all = { people: people, light: true };
    none = { people: [], light: false };
    self.start = JSON.stringify([all, none]);
    self.goal =  JSON.stringify([none, all]);

    return self;
};

BridgeTorch.prototype = Object.create(LeastCostSearch.prototype);
BridgeTorch.prototype.constructor = BridgeTorch;

BridgeTorch.prototype.sortCompare = function (a, b) {
    "use strict";
    return a - b;
};

BridgeTorch.prototype.isGoal = function (state) {
    "use strict";
    return this.goal === state;
};

BridgeTorch.prototype.successor = function (state) {
    "use strict";
    var oState, here, there, dir, numPeople, setsOf2, setsOf1,
        dict, self, setsOfCrossers;

    oState = JSON.parse(state);
    if (!oState[0].light) {
        dir = '<-';
        here = oState[1];
        there = oState[0];
    } else {
        dir = '->';
        here = oState[0];
        there = oState[1];
    }

    numPeople = here.people.length;
    setsOf2 = (numPeople > 1) ? MathExtras.combinations(numPeople, 2) : [];
    setsOf1 = (numPeople > 0) ? MathExtras.combinations(numPeople, 1) : [];

    setsOfCrossers = setsOf2.concat(setsOf1).map(
            function (set) {
                return set.map(
                    function (pIndex) {
                        // here.people array is zero-indexed
                        return here.people[pIndex - 1];
                    });
            });

    dict = {};
    self = this;

    setsOfCrossers.forEach(function (crossers) {
        var newThere = {}, newHere = {}, key;

        newHere.people = here.people.filter(
            function (person) {
                // creates the set: {here.people} - {crossers}
                return crossers.indexOf(person) === -1;
            }).sort(self.sortCompare);
        newHere.light = false;

        newThere.people = there.people.slice().
                            concat(crossers).sort(self.sortCompare);
        newThere.light = true;

        if (dir !== '->') {
            key = JSON.stringify([newThere, newHere]);
        } else {
            key = JSON.stringify([newHere, newThere]);
        }

        dict[key] = [JSON.stringify(crossers), dir];
    });

    return dict;
};

BridgeTorch.prototype.actionCost = function (action) {
    "use strict";
    return Math.max.apply(null, JSON.parse(action[0]));
};

BridgeTorch.prototype.pathCompare = function (path1, path2) {
    "use strict";
    return this.pathCost(path1) - this.pathCost(path2);
};

module.exports = BridgeTorch;
