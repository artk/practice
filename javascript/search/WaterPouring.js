/**
 * Created by art on 5/27/14.
 */

var ShortestPathSearch = require('./ShortestPathSearch.js');
var BasicQueue = require('../lib/BasicQueue.js');

function WaterPouring(X, Y, goal, startX, startY) {
    if (arguments.length < 3) {
        throw Error("Missing glass sizes and goal.");
    }

    var self = this instanceof WaterPouring ? this
               : Object.create(WaterPouring.prototype);

    ShortestPathSearch.call(self, new BasicQueue());

    self.X = X;
    self.Y = Y;
    self.startX = startX || 0;
    self.startY = startY || 0;
    self.start = JSON.stringify([startX, startY]);
    self.goal = new RegExp("\\[" + goal + ",\\d+]|\\[\\d+," + goal + "]");
    return self;
};

WaterPouring.prototype = Object.create(ShortestPathSearch.prototype);
WaterPouring.prototype.constructor = WaterPouring;

WaterPouring.prototype.isGoal = function (state) {
    return this.goal.test(state);
};


WaterPouring.prototype.successor = function (state) {
    var oState = JSON.parse(state);
    var x = oState[0], y = oState[1], X= this.X, Y = this.Y;

    var XtoY = JSON.stringify((y+x <= Y) ? [0, y+x] : [x-(Y-y), Y]),
        YtoX = JSON.stringify((x+y <= X) ? [x+y, 0] : [X, y-(X-x)]),
        fillX = JSON.stringify([X, y]),
        fillY = JSON.stringify([x, Y]),
        emptyX = JSON.stringify([0, y]),
        emptyY = JSON.stringify([x, 0]);

    var dict = {};

    dict[XtoY] = "X->Y";
    dict[YtoX] = "X<-Y";
    dict[fillX] = "fill X";
    dict[fillY] = "fill Y";
    dict[emptyX] = "empty X";
    dict[emptyY] = "empty Y";
    return dict;
};

module.exports = WaterPouring;
