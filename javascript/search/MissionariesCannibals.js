var zipWithAdd = require('../lib/zipWithAdd.js');
var zipWithSubtract = require('../lib/zipWithSubtract.js');
var BasicQueue = require('../lib/BasicQueue.js');
var ShortestPathSearch = require('./ShortestPathSearch.js');

function MissionariesCannibals(m, c) {

    var self = this instanceof MissionariesCannibals ? this
               : Object.create(MissionariesCannibals.prototype);

    ShortestPathSearch.call(self, new BasicQueue());

    if (typeof m === 'undefined') {
        m = c = 3;
    }
    else if (c === 'undefined') {
        c = m
    }
    else if ( m !== c) {
        throw Error('Unequal number of missionaries and cannibals');
    }

    // The puzzle is represented by an array of two arrays (starting and
    // destination banks).  Each bank is an array of 3 elements:
    //    number of missionaries, number of cannibals, and boat flag
    self.start = JSON.stringify([[m, c, 1], [0, 0, 0]]);
    self.goal = JSON.stringify([[0, 0, 0], [m, c, 1]]);
    self.maxPerGroup = c;

    return self;
}

MissionariesCannibals.actions = (function () {
    return [
        { boat: 'MM', state: [[2, 0, 1], [-2,  0, -1]] },
        { boat: 'CC', state: [[0, 2, 1], [ 0, -2, -1]] },
        { boat: 'MC', state: [[1, 1, 1], [-1, -1, -1]] },
        { boat: 'M',  state: [[1, 0, 1], [-1,  0, -1]] },
        { boat: 'C',  state: [[0, 1, 1], [ 0, -1, -1]] }
    ];
}());


MissionariesCannibals.prototype = Object.create(ShortestPathSearch.prototype);
MissionariesCannibals.prototype.constructor = MissionariesCannibals;


MissionariesCannibals.prototype.isGoal = function (state) {
    return state === this.goal;
};

MissionariesCannibals.prototype.successor = function (state) {
    var binOp, dir, oState, boat, src, dest, self, dict;

    boat = 2;   // boat index
    src = 0;    // source index (starting bank)
    dest = 1;   // destination (destination river bank)
    oState = JSON.parse(state);

    if (oState[src][boat] === 1) {
        binOp = zipWithSubtract;
        dir = '->';
    }
    else {
        binOp = zipWithAdd;
        dir = '<-';
    }

    self = this;
    dict = {};

    MissionariesCannibals.actions.forEach(function (action) {
        var M = 0, C = 1;  // missionaries and cannibals indices
        var nextState = [ binOp(oState[src], action.state[src]),
                          binOp(oState[dest], action.state[dest]) ];

        var bankSrc = nextState[src], bankDest = nextState[dest];

        // can't have more cannibals than missionaries on each bank
        if (bankSrc[M] > 0 && bankSrc[M] < bankSrc[C]) {
            return;
        }
        else if (bankDest[M] > 0 && bankDest[M] < bankDest[C]) {
            return;
        }

        for (var i=0; i < bankSrc.length; i++) {
            if (bankSrc[i] < 0 || bankSrc[i] > self.maxPerGroup)
                return;
        }

        for (i=0; i < bankDest.length; i++) {
            if (bankDest[i] < 0 || bankDest[i] > self.maxPerGroup)
                return;
        }

        dict[JSON.stringify(nextState)] = dir + action.boat;
    });

    return dict;
};


module.exports = MissionariesCannibals

