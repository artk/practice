function LeastCostSearch(queue) {
    var self;

    if (queue === 'undefined') {
        throw new Error("queue object is missing");
    }

    self = this instanceof LeastCostSearch ? this : Object.create(LeastCostSearch.prototype);
    self.frontier = queue;

    return self;
}

LeastCostSearch.prototype.execute = function() {
    /*
        path - an array of states and actions;
              [state, action, state, ..., action, state]
        successor - method that takes a state as an argument and returns a dictionary
                    of state/action (key/value) pairs.
        isGoal - method that takes a state as argument and returns a boolean

        successor and isGoal are implemented by child objects
     */

    var path, path2, state1, pCost, state, action, totalCost,
        stateActionTuples;
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    var explored = {}, frontier = this.frontier;

    frontier.add([this.start]);  // this.start is set by child object

    while (!frontier.isEmpty()) {

        path = frontier.remove();
        state1 = this.pathFinalState(path);

        if (this.isGoal(state1)) {
            return path;
        }

        explored[state1] = true;
        pCost = this.pathCost(path);
        stateActionTuples = this.successor(state1);

        for (state in stateActionTuples) {
            if (hasOwnProperty.call(stateActionTuples, state)) {
                action = stateActionTuples[state];

                if (!(state in explored)) {
                    totalCost = pCost + this.actionCost(action);
                    path2 = path.slice();
                    path2.push({action : action, cost : totalCost});
                    path2.push(state);
                    frontier.add(path2);
                }
            }
        }
    }

    return [];
};

LeastCostSearch.prototype.pathFinalState = function (path) {
    return path[path.length-1];
};

LeastCostSearch.prototype.pathCost = function (path) {
    if (path.length < 3) return 0;
    else return path[path.length-2].cost;
};

module.exports = LeastCostSearch;
