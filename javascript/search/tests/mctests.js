// replace later with unit tests

var MissionariesCannibals = require('../MissionariesCannibals.js');

var mc = new MissionariesCannibals(3, 3);
console.log("3 Missionaries, 3 Cannibals");
console.log(mc.execute());
console.log();
console.log("2 Missionaries, 2 Cannibals");
mc = new MissionariesCannibals(2, 2);
console.log(mc.execute());
console.log();
console.log("5 Missionaries, 5 Cannibals");
mc = new MissionariesCannibals(5, 5);
console.log(mc.execute());
