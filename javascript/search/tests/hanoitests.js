// replace later with unit tests

var TowerHanoi = require('../TowerHanoi.js');

var disks = 4;
var hanoi = new TowerHanoi(disks);
var solution = hanoi.execute();

console.log(solution);
console.log(disks + " disks , " +  (solution.length-1)/2 + " steps.");
console.log(hanoi.constructor);
console.log(Object.getPrototypeOf(hanoi));

hanoi.successor(JSON.stringify([[4], [3], [1,2]]));
