## NOTES ##

In 2012, [Peter Norvig](http://norvig.com) taught an online course called [Design of Computer Programs](https://www.udacity.com/course/cs212). Unit 4 of this course was about Search. Mr. Norvig used python to implement the search concepts.

I reviewed the Search lesson by implementing the following programs in Javascript:

1. Missionaries-Cannibals river crossing
2. Bridge Torch crossing
3. Water Pouring with 2 glasses
4. Tower of Hanoi (not covered by class, just wanted to write it)
