/**
 * Created by art on 5/31/14.
 */

var BasicQueue = require('../lib/BasicQueue.js');
var ShortestPathSearch = require('./ShortestPathSearch.js');

function TowerHanoi(plates) {
    var self, pole, i;

    if (typeof plates !== 'number') {
        throw Error("plates type must be a number.");
    }

    self = this instanceof TowerHanoi ? this
               : Object.create(TowerHanoi.prototype);

    ShortestPathSearch.call(self, new BasicQueue());
    pole = [];
    pole.length = plates;

    for (i=0; i < plates; i++) {
        pole[i] = i+1;
    }

    self.plates = plates;

    // Tower of Hanoi is represented by an array or arrays:
    //   [[src] [aux] [dest]]
    self.start = JSON.stringify([pole, [], []]);
    self.goal = JSON.stringify([[], [], pole]);

    return self;
}


TowerHanoi.prototype = Object.create(ShortestPathSearch.prototype);
TowerHanoi.prototype.constructor = TowerHanoi;

TowerHanoi.prototype.isGoal = function (state) {
    return state === this.goal;
};


TowerHanoi.prototype.successor = function (state) {
    var dict = {},  top = 0;
    var direction, plate, plateI, plateJ, fakePlate = this.plates+1;
    var tower = JSON.parse(state);
    var poles = tower.length;

    for (var i=0; i < poles-1; i++) {
        plateI = tower[i].length === 0 ? fakePlate : tower[i][top];

        for (var j=i+1; j < poles; j++) {
            var hanoi = [tower[0].slice(), tower[1].slice(), tower[2].slice()];
            plateJ = hanoi[j].length === 0 ? fakePlate : hanoi[j][top];

            if (plateI < plateJ) {
                hanoi[j].unshift(hanoi[i].shift());
                direction = '->';
                plate = plateI;
            }
            else if (plateI > plateJ) {
                hanoi[i].unshift(hanoi[j].shift());
                direction = '<-';
                plate = plateJ;
            }
            else {
                continue;
            }

            dict[JSON.stringify(hanoi)] = plate + ", P" + (i+1) + direction + "P" + (j+1);
        }
    }

    return dict;
};

module.exports = TowerHanoi;
