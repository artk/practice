/**
 * Quick implementation of trie for boggle words generation
 * @returns {{add: add, contains: contains, isPrefix: isPrefix, matchFound: matchFound, getRoot: getRoot}}
 */
var trie = function () {

    "use strict"
    var trieNode = function() {
        "use strict"
        return {
            item : {},
            value : -1
        };
    };
    var ROOT_SYMBOL = '\\';
    var root = trieNode();
    var elem = 0;

    root.item[ROOT_SYMBOL] = trieNode();

    function add(word, node) {
        if (word.length == 0) {
            node.value = ++elem;
            return;
        }

        var letter = word.charAt(0);

        if (letter in node.item) {
            add(word.substring(1), node.item[letter]);
        }
        else {
            node.item[letter] = trieNode();
            add(word.substring(1), node.item[letter]);
        }
    }

    function contains(word, trie) {
        if (word.length == 0) {
            return trie;
        }

        var letter = word.charAt(0);

        if (letter in trie.item) {
            return contains(word.substring(1), trie.item[letter]);
        }
        else
            return null;
    }

    return {
        add : function (word) {
            add(word, root.item[ROOT_SYMBOL]);
        },

        contains : function(word) {
            return contains(word.toLowerCase(), root.item['\\']) != null;
        },

        isPrefix : function(word) {
            var node = contains(word.toLowerCase(), root.item['\\']);
            if (node != null) {
                return node.value == -1;
            }
            return false;
        },

        matchFound : function(word) {
            var  node = contains(word.toLowerCase(), root.item['\\']);
            if (node != null) {
                return node.value > 0;
            }
            return false;
        },

        getRoot : function() {
            return root;
        }
    };

};

module.exports = trie;