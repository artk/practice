var BitMap = require('../lib/BitMap.js');

function BogglePaths(graph, pathValidator) {
    var self = this instanceof BogglePaths ? this : Object.create(BogglePaths.prototype);

    self.graph = function() { return graph; }
    self.pathValidator = function() { return pathValidator;}

    return self;
};

BogglePaths.prototype.find = function (startVertex) {

    function dfs(G, v, visited, path, pathValidator) {
        var neighbors = G.adj(v);

        path.push(v);
        visited.set(v);

        neighbors.forEach(function (w) {
            var tempPath;

            if (visited.isSet(w)) {
                return;
            }

            tempPath = path.slice();
            tempPath.push(w);

            if (!pathValidator.isValid(tempPath)) {
                return;
            }

            dfs(G, w, visited, path, pathValidator);
        });

        visited.unset(v);
        path.pop();
    }

    var path = [];
    var visited = new BitMap(this.graph().V());
    dfs(this.graph(), startVertex, visited, path, this.pathValidator());
};

module.exports = BogglePaths;
