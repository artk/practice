var bogglePathValidator = function(trieDict, letters) {
    var matchSubcribers = [];
    var prefixSubcribers = [];

    return {
        isValid : function (path) {
            var word = path.map(function (x) { return letters[x]}).join('');
            if (!trieDict.contains(word)) {
                return false;
            }
            var message = { word: word, path: path.join("-") };
            if (trieDict.matchFound(word)) {
                matchSubcribers.forEach(function(fn) {
                    if (typeof fn === 'function') {
                        fn(message);
                    }
                });
            }
            else {
                prefixSubcribers.forEach(function(fn) {
                    if (typeof fn === 'function') {
                        fn(message);
                    }
                });
            }

            return true;
        },

        addSubscriber : function (type, fn) {
            var msg = type.trim().toLowerCase();
            if (msg === "prefix") {
                prefixSubcribers.push(fn);
            }
            else if (msg === 'match') {
                matchSubcribers.push(fn);
            }
        },

        finish : function () {
            var message = { word: undefined, path: undefined };

            matchSubcribers.forEach(function(fn) {
                if (typeof fn === 'function') {
                    fn(message);
                }
            });

            prefixSubcribers.forEach(function(fn) {
                if (typeof fn === 'function') {
                    fn(message);
                }
            });

        }
    }
};

module.exports = bogglePathValidator;