## Notes ##

1. The web-app version of this self-playing Boggle word search program is available [here](http://www.beijingbayou.com/boggle.html).

2. bogglewords.json contains 4-16 letter-words found in the crosswords file from [Moby Words](http://icon.shef.ac.uk/Moby/mwords.html).

