// Sample Program:  nodejs command line (REPL) program to find Boggle words
var fs = require('fs');
var Boggle = require('./Boggle.js');
var bogglePathValidator = require('./bogglePathValidator.js');
var trie = require('./trie.js');

fs.readFile('./bogglewords.json', 'utf-8', function(err, data) {
    var wordsTree, words, letters, pathValidator, boggle;

    wordsTree = trie();
    words = JSON.parse(data);

    words.forEach(function(word) {
        wordsTree.add(word);
    });

    letters = ['f', 'n', 'h', 'd', 'a', 'y', 'a', 'c', 'v', 'o', 'r', 'd', 'e', 'l', 'r', 'e' ];
    pathValidator = bogglePathValidator(wordsTree, letters);

    pathValidator.addSubscriber('match', function (message) {
        if (message.word === undefined) {
            return;
        }

        console.log(message.word, message.path);
    });

    boggle = new Boggle(pathValidator);
    boggle.findWords(letters);
});

