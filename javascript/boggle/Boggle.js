var Graph = require('../lib/Graph.js');
var MathExtras = require('../lib/MathExtras.js');
var matrix = require('../lib/matrix.js');
var BogglePaths = require('./BogglePaths.js');

function Boggle(pathValidator) {
    var self = this instanceof Boggle ? this : Object.create(Boggle.prototype);

    self.pathValidator = function() {
        return pathValidator;
    };

    return self;
}


Boggle.buildGraph = function (numLetters) {
    var ROWS, COLS, mat, neighbors, i, j, k;

    if (!MathExtras.isPerfectSquare(numLetters) || numLetters < 16 ) {
        throw new Error("Number of letters must be a perfect square and >= 16");
    }

    ROWS = Math.sqrt(numLetters);
    COLS = ROWS;
    mat = matrix(ROWS, COLS);


    function genNeighbors(matrix, row, col) {
        var adjacents = [
            {row: row-1, col: col-1},
            {row: row-1, col: col},
            {row: row-1, col: col+1},
            {row: row, col: col-1},
            {row: row, col: col+1},
            {row: row+1, col: col-1},
            {row: row+1, col: col},
            {row: row+1, col: col+1}
        ];

        return adjacents.
            filter(function(pos) {
                return pos.row >=0 &&
                    pos.row < ROWS &&
                    pos.col >= 0 &&
                    pos.col < COLS;
            }).
            map(function(pos) {
                return mat[pos.row][pos.col];
            });
    }


    for (i= 0, n=0; i < ROWS; i++) {
        for (j=0; j < COLS; j++)  {
            mat[i][j] = n++;
        }
    }

    var g = new Graph();

    for (i=0; i < ROWS; i++) {
        for (j=0; j < COLS; j++) {
            neighbors = genNeighbors(mat, i, j);

            for (k=0; k < neighbors.length; k++) {
                g.addEdge(mat[i][j], neighbors[k]);
            }
        }
    }

    return g;
};


Boggle.prototype.findWords = function(letters) {

    var graph = Boggle.buildGraph(letters.length);
    var bogglePaths = new BogglePaths(graph, this.pathValidator());
    var vertices = graph.V();

    for (var v=0; v < vertices; v++) {
        bogglePaths.find(v);
    }

    this.pathValidator().finish();
};

module.exports = Boggle;
