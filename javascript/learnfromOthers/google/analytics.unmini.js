/**
 * http://www.google-analytics.com/analytics.js (retrieved on 2014-07-01)
 *
 * beautified by http://jsbeautifier.org
 *
 *
 * Research references:
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Location
 * https://developer.mozilla.org/en-US/docs/Web/API/URLUtils.search
 * https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
 * etc.
 */
(function () {
    /*
    var aa = encodeURIComponent,
        f = window,
        ba = setTimeout,
        n = Math;
    */
    function ak_setObjectName(obj, name) {      // function fa(a, b) {

        return obj.name = name;
    }

    function ak_setHrefAttr(a, b) {      //function Pc(a, b) {
        return a.href = b
    }

    /*
    var p = "push",
        h = "hash",
        s = "test",
        ha = "slice",
        Qc = "replace",
        q = "data",
        r = "cookie",
        Cc = "charAt",
        t = "indexOf",
        m = "match",
        ia = "defaultValue",
        xc = "send",
        ja = "port",
        u = "createElement",
        id = "setAttribute",
        v = "name",
        da = "getTime",
        x = "host",
        y = "length",
        z = "prototype",
        la = "clientWidth",
        A = "split",
        B = "location",
        ra = "hasOwnProperty",
        ma = "hostname",
        ga = "search",
        jd = "target",
        C = "call",
        E = "protocol",
        na = "clientHeight",
        Ab = "href",
        F = "substring",
        kd = "action",
        G = "apply",
        oa = "navigator",
        Ub = "parentNode",
        H = "join",
        I = "toLowerCase";
    */


    var usage = new function () { //var pa = new function () {
        var a = [];
        this.set = function (b) {
            a[b] = !0;
        };
        this.M = function () {
            for (var b = [], c = 0; c < a.length; c++) a[c] && (b[Math.floor(c / 6)] = b[Math.floor(c / 6)] ^ 1 << c % 6);
            for (c = 0; c < b.length; c++) b[c] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_" .charAt(b[c] || 0);
            return b.join("") + "~";
        }
    };

    function ak_setFeatureUsed(feature) {  //function J(a) {
        usage.set(feature);
    }

    function ak_isfunction(a) { // function K(a) {
        return "function" == typeof a;
    }

    function ak_isArray(a) { // function vd(a) {
        return "[object Array]" == Object.prototype.toString.call(Object(a));
    }

    function ak_isString(a) { //function qa(a) {
        return void 0 != a && -1 < (a.constructor + "").indexOf("String");
    }

    function ak_indexOf(a, b) { //function ea(a, b) {
        return 0 == a.indexOf(b);
    }

    function ak_trim(a) { //function Lc(a) {
        return a ? a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "") : "";
    }

    function ak_fireImageBeacon(url) { //function Ca(a) {
        var b = document.createElement("img");
        b.width = 1;
        b.height = 1;
        b.src = url;
        return b;
    }

    function emptyFn() {}  //function L() {}

    function ak_encodeURIComponent(a) {  //function sa(a) {
        if (encodeURIComponent instanceof Function) return encodeURIComponent(a);
        ak_setFeatureUsed(28);
        return a;
    }

    function ak_encodeParens(a) {  //function ka(a) {
        return ak_encodeURIComponent(a).replace(/\(/g, "%28").replace(/\)/g, "%29");
    }

    var ak_addEvent = function (target, event, listener, useCapture) { //var ta = function (a, b, c, d) {
            try {
                target.addEventListener ? target.addEventListener(event, listener, !!useCapture) : target.attachEvent && target.attachEvent("on" + event, listener);
            } catch (e) {
                ak_setFeatureUsed(27);
            }
        },
        ak_removeEvent = function (target, event, listener) { //ua = function (a, b, c) {
            target.removeEventListener ? target.removeEventListener(event, listener, !1) : target.detachEvent && target.detachEvent("on" + event, listener);
        };

    function ak_loadScript(a, b) { //function vc(a, b) {
        if (a) {
            var c = document.createElement("script");
            c.type = "text/javascript";
            c.async = !0;
            c.src = a;
            c.id = b;
            var d = document.getElementsByTagName("script")[0];
            d.parentNode.insertBefore(c, d);
        }
    }

    function getProtocol() { //function D() {
        return forceSSL || "https:" == document.location.protocol ? "https:" : "http:";
    }

    function ak_getHostname() { //function eb() {
        var a = "" + document.location.hostname;
        return 0 == a.indexOf("www.") ? a.substring(4) : a;
    }

    function ak_getReferrer(alwaysSendReferrer) { // function va(a) {
        var a;
        var b = document.referrer;
        if (/^https?:\/\//i.test(b)) {
            if (alwaysSendReferrer) return b;
            a = "//" + document.location.hostname;
            var c = b.indexOf(alwaysSendReferrer);
            if (5 == c || 6 == c)
                if (a = b.charAt(c + a.length), "/" == a || "?" == a || "" == a || ":" == a) return;
            return b;
        }
    }

    function wa(a, b) {
        if (1 == b.length && null != b[0] && "object" === typeof b[0]) return b[0];
        for (var c = {}, d = Math.min(a.length + 1, b.length), e = 0; e < d; e++)
            if ("object" === typeof b[e]) {

                for (var g in b[e]) b[e].hasOwnProperty(g) && (c[g] = b[e][g]);

                break;
            } else e < a.length && (c[a[e]] = b[e]);
        return c;
    }

    var GADict = function () { // var N = function () {
        this.keys = [];
        this.w = {};
        this.m = {};
    };

    GADict.prototype.set = function (a, b, c) {
        this.keys.push(a);
        c ? this.m[":" + a] = b : this.w[":" + a] = b;
    };

    GADict.prototype.get = function (a) {
        return this.m.hasOwnProperty(":" + a) ? this.m[":" + a] : this.w[":" + a];
    };

    GADict.prototype.map = function (fn) {
        for (var b = 0; b < this.keys.length; b++) {
            var c = this.keys[b],
                d = this.get(c);

            d && fn(c, d);
        }
    };

    /*
    var O = window,
        M = document,
    */

    var xa = function (a) {
            var b = window._gaUserPrefs;
            if (b && b.ioo && b.ioo() || a && !0 === window["ga-disable-" + a]) return !0;
            try {
                var c = window.external;
                if (c && c._gaUserPrefs && "oo" == c._gaUserPrefs) return !0;
            } catch (d) {}
            return !1;
        },
        ak_delay = function (a) { // fb = function (a) {
            setTimeout(a, 100);
        },
        ak_getCookies = function (name) {  //ya = function (a) {
            var a, b = [],
                c = document.cookie.split(";");  // array of all cookies
            a = new RegExp("^\\s*" + name + "=\\s*(.*?)\\s*$");
            for (var d = 0; d < c.length; d++) {
                var e = c[d].match(a);
                e && b.push(e[1]);
            }
            return b;
        },
        writeCookie = function (name, val, path, domain, gaId, expires) { //zc = function (a, b, c, d, e, g) {
            var a, c, d, e;

            e = xa(gaId) ? !1 : ak_doubleclickDomain.test(document.location.hostname) ||
                "/" == path && ak_googleDomain.test(domain) ? !1 : !0;

            if (!e) return !1;

            val && 1200 < val.length && (val = val.substring(0, 1200), ak_setFeatureUsed(24));
            c = name + "=" + val + "; path=" + path + "; ";
            expires && (c += "expires=" + (new Date((new Date).getTime() + expires)).toGMTString() + "; ");
            domain && "none" != domain && (c += "domain=" + domain + ";");
            d = document.cookie;  // get cookies
            document.cookie = c;   // add cookie/update cookie
            if (!(d = d != document.cookie)) t: {
                a = ak_getCookies(name);
                for (d = 0; d < a.length; d++)
                    if (val == a[d]) {
                        d = !0;
                        break t;
                    }
                d = !1;  // browser does not allow cookies
            }
            return d;
        },
        ak_googleDomain = new RegExp(/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/),  //za = new RegExp(/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/),
        ak_doubleclickDomain = new RegExp(/(^|\.)doubleclick\.net$/i), //Aa = new RegExp(/(^|\.)doubleclick\.net$/i),
        Mc = function () {
            // looks like a bug: a.version; should be a.appVersion
            for (var a = window.navigator, a = a.appName + a.version + a.platform + a.userAgent + (document.cookie ? document.cookie : "") + (document.referrer ? document.referrer : ""), b = a.length, c = window.history.length; 0 < c;) a += c-- ^ b++;
            return La(a);
        };

    var ak_gaUrlDomain = function () { //var oc = function () {
            return getProtocol() + "//www.google-analytics.com";
        },
        ak_ErrDataOverflow = function (a) { //Da = function (a) {
            ak_setObjectName(this, "len");
            this.message = a + "-8192";
        },
        ak_ErrFirefoxVersionTooOld = function (a) {  // //Ea = function (a) {
            ak_setObjectName(this, "ff2post");
            this.message = a + "-2036";
        },
        GaSendData = function (data, callback) { // Ga = function (a,b) {
            callback = callback || emptyFn;
            if (2036 >= data.length) ak_beaconCollectStats(data, callback);
            else if (8192 >= a.length) {
                var c = callback;
                if (0 <= window.navigator.userAgent.indexOf("Firefox") && ![].reduce) {
                    throw new ak_ErrFirefoxVersionTooOld(data.length);
                }
                ak_CORSCollectStats(data, c) || ak_IECORSCollectStats(data, c) || ak_iFrameCollectStats(data, c) || c()
            } else throw new ak_ErrDataOverflow(data.length);
        },
        ak_beaconCollectStats = function (queryString, callback) { // wc = function (a, b)
            var c = ak_fireImageBeacon(ak_gaUrlDomain() + "/collect?" + queryString);
            c.onload = c.onerror = function () {
                c.onload = null;
                c.onerror = null;
                callback();
            }
        },
        ak_IECORSCollectStats = function (data, callback) { //xd = function (a, b) {
            var c;
            c = window.XDomainRequest;
            if (!c) return !1;
            c = new c;
            c.open("POST", ak_gaUrlDomain() + "/collect");
            c.onerror = function () {
                callback();
            };
            c.onload = callback;
            c.send(data);
            return !0;
        },
        ak_CORSCollectStats = function(data, callback) { // wd = function (a, b) {
            var c = window.XMLHttpRequest;
            if (!c) return !1;
            var d = new c;
            if (!("withCredentials" in d)) return !1;
            d.open("POST", ak_gaUrlDomain() + "/collect", !0);
            d.withCredentials = !0;
            d.setRequestHeader("Content-Type", "text/plain");
            d.onreadystatechange = function () {
                4 == d.readyState && (callback(), d = null);
            };
            d.send(data);
            return !0;
        },
        ak_iFrameCollectStats = function (a, b) { // Fa = function (a, b) {
            if (!document.body) return ak_delay(function () {
                Fa(a, b);
            }), !0;
            a = encodeURIComponent(a);
            try {
                var c = document.createElement('<iframe name="' +
                    a + '"></iframe>');
            } catch (d) {
                c = document.createElement("iframe"), ak_setObjectName(c, a);
            }
            c.height = "0";
            c.width = "0";
            c.style.display = "none";
            c.style.visibility = "hidden";
            var e = document.location,
                e = ak_gaUrlDomain() + "/analytics_iframe.html#" + encodeURIComponent(e.protocol + "//" + e.host + "/favicon.ico"),
                g = function () {
                    c.src = "";
                    c.parentNode && c.parentNode.removeChild(c);
                };
            ak_addEvent(window, "beforeunload", g);
            var ca = !1,
                l = 0,
                k = function () {
                    if (!ca) {
                        try {
                            if (9 < l || c.contentWindow.location.host == document.location.host) {
                                ca = !0;
                                g();
                                ak_removeEvent(window, "beforeunload", g);
                                b();
                                return;
                            }
                        } catch (a) {}
                        l++;
                        setTimeout(k, 200);
                    }
                };
            ak_addEvent(c, "load", k);
            document.body.appendChild(c);
            c.src = e;
            return !0;
        };

    var GAFilters = function () { //var Ha = function () {
        this.t = [];
    };

    GAFilters.prototype.add = function (a) {
        this.t.push(a);
    };

    GAFilters.prototype.D = function (a) {
        try {
            for (var b = 0; b < this.t.length; b++) {
                var c = a.get(this.t[b]);
                c && ak_isfunction(c) && c.call(window, a);
            }
        } catch (d) {
            console.log("ola exception ", d);
        }
        b = a.get(Ia);   //get hitCallback value
        b != emptyFn && ak_isfunction(b) && (a.set(Ia, emptyFn, !0), setTimeout(b, 10));
    };


    /*
     *   https://developers.google.com/analytics/devguides/collection/analyticsjs/tasks?hl=nn-NO
     */

    /**
     * Samples out visitors based on the sampleRate setting for this tracker.
     * @param a
     */
    function samplerTask(a) { //function Ja(a) {
        if (100 != a.get(Ka) && La(getStringValue(a, Q)) % 1E4 >= 100 * getNumericValue(a, Ka)) throw "abort";
    }

    function _oot(a) { //function Ma(a) {
        if (xa(getStringValue(a, Na))) throw "abort";
    }

    /**
     * Aborts the request if the page protocol is not http or https.
     */
    function checkProtocolTask() { //function Oa() {
        var a = document.location.protocol;
        if ("http:" != a && "https:" != a) throw "abort";
    }

    /**
     * Builds a measurement protocol request string and stores it in the hitPayload field.
     * @param a
     */
    function buildHitTask(a) { //function Pa(a) {
        try {
            // set CORS or sendBeacon feature if available
            window.XMLHttpRequest && "withCredentials" in new window.XMLHttpRequest ? ak_setFeatureUsed(40) : window.XDomainRequest && ak_setFeatureUsed(41), window.navigator.sendBeacon && ak_setFeatureUsed(42)
        } catch (b) {}
        a.set(Ac, getNumericValue(a, Ac) + 1);
        var c = [];
        GAFieldsST.map(function (b, e) {
            if (e.p) {
                var g = a.get(b);
                void 0 != g && g != e.defaultValue && ("boolean" == typeof g && (g *= 1), c.push(e.p + "=" + ak_encodeURIComponent("" + g)))
            }
        });
        c.push("z=" + ak_getRandom());
        a.set(Ra, c.join("&"), !0);
    }

    /**
     * Transmits the measurement protocol request stored in the hitPayload field to Google Analytics servers.
     * @param a
     */
    function sendHitTask(a) { //function Sa(a) {
        GaSendData(getStringValue(a, Ra), a.get(Ia));
        a.set(Ia, emptyFn, !0);
    }

    function ceTask(a) { //function Hc(a) {
        var b = window.gaData;
        b && (b.expId && a.set(Nc, b.expId), b.expVar && a.set(Oc, b.expVar));
    }

    /**
     * Aborts the request if the page is only being rendered to generate a 'Top Sites' thumbnail for Safari.
     */
    function previewTask() { //function cd() {
        if (window.navigator && "preview" == window.navigator.loadPurpose) throw "abort";
    }

    function devIdTask(a) { //function yd(a) {
        var b = window.gaDevIds;
        ak_isArray(b) && 0 != b.length && a.set("&did", b.join(","), !0);
    }

    /**
     * Aborts the request if required fields are missing or invalid.
     * @param a
     */
    function validationTask(a) { //function vb(a) {
        if (!a.get(Na)) throw "abort";
    }

    var ak_random = function () {
    //var hd = function () {
            //  8th Mersenne prime,  http://en.wikipedia.org/wiki/2147483647
            return Math.round(2147483647 * Math.random());
        },
        ak_getRandom = function () {  //Bd = function () {
            try {
            // https://developer.mozilla.org/en-US/docs/Web/API/Uint32Array
                var a = new Uint32Array(1);
                window.crypto.getRandomValues(a);
                return a[0] & 2147483647;
            } catch (b) {
                return ak_random();
            }
        };

    function _rlt(a) { //function Ta(a) {
        var b, c, d, e;

        b = getNumericValue(a, Ua);
        500 <= b && ak_setFeatureUsed(15);
        c = getStringValue(a, Va);
        if ("transaction" != c && "item" != c) {
            c = getNumericValue(a, Wa),
            d = (new Date).getTime(),
            e = getNumericValue(a, Xa);
            0 == e && a.set(Xa, d);
            e = Math.round(2 * (d - e) / 1E3);
            0 < e && (c = Math.min(c + e, 20), a.set(Xa, d));

            if (0 >= c) throw "abort";

            a.set(Wa, --c);
        }
        a.set(Ua, ++b);
    }

    var Ya = function () {
            // constructor function
            this.data = new GADict;
        },
        GAFieldsST = new GADict,  // ST - symbol table //Qa = new GADict,
        Za = [];

    Ya.prototype.get = function (a) {
        var b = ak_getGAField(a),
            c = this.data.get(a);

        b && void 0 == c && (c = ak_isfunction(b.defaultValue) ? b.defaultValue() : b.defaultValue);
        return b && b.n ? b.n(this, a, c) : c;
    };

    var getStringValue = function (a, b) {  //var P = function (a, b) {
            var c = a.get(b);
            return void 0 == c ? "" : "" + c;
        },
        getNumericValue = function (a, b) { //R = function (a, b) {
            var c = a.get(b);
            return void 0 == c || "" === c ? 0 : 1 * c;
        };

    Ya.prototype.set = function (a, b, c) {
        if (a)
            if ("object" == typeof a)
                for (var d in a) a.hasOwnProperty(d) && ab(this, d, a[d], c);
            else ab(this, a, b, c);
    };

    var ab = function (obj, name, c, d) { //var ab = function (a, b, c, d) {
            if (void 0 != c) switch (name) {
            case Na:   // trackingId
                isTrackingIdValid.test(c)
            }
            var e = ak_getGAField(name);
            e && e.o ? e.o(obj, name, c, d) : obj.data.set(name, c, d);
        },
        GAField = function (name, protocolParm, val, getFn, setFn) { //bb = function (name, b, c, d, e) {
            // see https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference
            ak_setObjectName(this, name);
            this.p = protocolParm;
            this.n = getFn;
            this.o = setFn;
            this.defaultValue = val;
        },
        ak_getGAField = function (name) { //$a = function (a) {
            var b = GAFieldsST.get(name);
            if (!b)
                for (var c = 0; c < Za.length; c++) {
                    var d = Za[c],  // get regex
                        e = d[0].exec(name);
                    if (e) {
                        b = d[1](e);  // call function tied to d[0] regex
                        GAFieldsST.set(b.name, b);
                        break;
                    }
                }
            return b;
        },
        yc = function(protocolParm) { //function (a) {
            var b;
            GAFieldsST.map(function (c, d) {
                d.p == protocolParm && (b = d);
            });
            return b && b.name;
        },
        createGAField = function (field, protocolParm, val, d, e) { //S = function (a, b, c, d, e) {
            // https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference
            field = new GAField(field, protocolParm, val, d, e);
            GAFieldsST.set(field.name, field);
            return field.name;
        },
        createGAFieldWithRegex = function (a, cbCreateGAField) { //cb = function (a, b) {
            Za.push([new RegExp("^" + a + "$"), cbCreateGAField]);
        },
        createGAFieldNoFunctions = function (gaField, protocolParm, val) { //T = function (a, b, c) {
            return createGAField(gaField, protocolParm, val, void 0, doNothingFn);
        },
        doNothingFn = function () {};     //db = function () {};

    var gb = ak_isString(window.GoogleAnalyticsObject) && ak_trim(window.GoogleAnalyticsObject) || "ga",
        forceSSL = !1, //Ba = !1,
        hb = createGAFieldNoFunctions("apiVersion", "v"),
        ib = createGAFieldNoFunctions("clientVersion", "_v");

    createGAField("anonymizeIp", "aip");

    var jb = createGAField("adSenseId", "a"),
        Va = createGAField("hitType", "t"),
        Ia = createGAField("hitCallback"),
        Ra = createGAField("hitPayload");

    createGAField("nonInteraction", "ni");
    createGAField("currencyCode", "cu");
    createGAField("sessionControl", "sc", "");
    createGAField("queueTime", "qt");

    var Ac = createGAField("_s", "_s");

    createGAField("screenName", "cd");

    var kb = createGAField("location", "dl", ""),
        lb = createGAField("referrer", "dr"),
        mb = createGAField("page", "dp", "");

    createGAField("hostname", "dh");

    var nb = createGAField("language", "ul"),
        ob = createGAField("encoding", "de");

    createGAField("title", "dt", function () {
        return document.title || void 0;
    });

    createGAFieldWithRegex("contentGroup([0-9]+)", function (a) {
        return new GAField(a[0], "cg" + a[1])
    });

    var pb = createGAField("screenColors", "sd"),
        qb = createGAField("screenResolution", "sr"),
        rb = createGAField("viewportSize", "vp"),
        sb = createGAField("javaEnabled", "je"),
        tb = createGAField("flashVersion", "fl");

    createGAField("campaignId", "ci");
    createGAField("campaignName", "cn");
    createGAField("campaignSource", "cs");
    createGAField("campaignMedium", "cm");
    createGAField("campaignKeyword", "ck");
    createGAField("campaignContent", "cc");

    var ub = createGAField("eventCategory", "ec"),
        xb = createGAField("eventAction", "ea"),
        yb = createGAField("eventLabel", "el"),
        zb = createGAField("eventValue", "ev"),
        Bb = createGAField("socialNetwork", "sn"),
        Cb = createGAField("socialAction", "sa"),
        Db = createGAField("socialTarget", "st"),
        Eb = createGAField("l1", "plt"),
        Fb = createGAField("l2", "pdt"),
        Gb = createGAField("l3", "dns"),
        Hb = createGAField("l4", "rrt"),
        Ib = createGAField("l5", "srt"),
        Jb = createGAField("l6", "tcp"),
        Kb = createGAField("l7", "dit"),
        Lb = createGAField("l8", "clt"),
        Mb = createGAField("timingCategory", "utc"),
        Nb = createGAField("timingVar", "utv"),
        Ob = createGAField("timingLabel", "utl"),
        Pb = createGAField("timingValue", "utt");

    createGAField("appName", "an");
    createGAField("appVersion", "av", "");
    createGAField("appId", "aid", "");
    createGAField("appInstallerId", "aiid", "");
    createGAField("exDescription", "exd");
    createGAField("exFatal", "exf");

    var Nc = createGAField("expId", "xid"),
        Oc = createGAField("expVar", "xvar"),
        Rc = createGAField("_utma", "_utma"),
        Sc = createGAField("_utmz", "_utmz"),
        Tc = createGAField("_utmht", "_utmht"),
        Ua = createGAField("_hc", void 0, 0),
        Xa = createGAField("_ti", void 0, 0),
        Wa = createGAField("_to", void 0, 20);

    createGAFieldWithRegex("dimension([0-9]+)", function (a) {
        return new GAField(a[0], "cd" + a[1]);
    });

    createGAFieldWithRegex("metric([0-9]+)", function (a) {
        return new GAField(a[0], "cm" + a[1]);
    });

    createGAField("linkerParam", void 0, void 0, Bc, doNothingFn);

    var ld = createGAField("usage", "_u", void 0,
                           function () { return usage.M(); },
                           doNothingFn);

    createGAField("forceSSL", void 0, void 0, function () {
        return forceSSL; },
        function (obj, fieldName, value) {
            ak_setFeatureUsed(34);
            forceSSL = !!value;
        }
    );

    var ed = createGAField("_j1", "jid");

    createGAField("_j2", "gjid");

    createGAFieldWithRegex("\\&(.*)", function (a) {
        var b = new GAField(a[0], a[1]),
            c = yc(a[0].substring(1));
        c && (b.n = function (a) {
                        return a.get(c)
                    }, b.o = function (a, b, g, ca) {
                        a.set(c, g, ca)
                    }, b.p = void 0);
        return b;
    });

    var Qb = createGAFieldNoFunctions("_oot"),
        dd = createGAField("previewTask"),
        Rb = createGAField("checkProtocolTask"),
        md = createGAField("validationTask"),
        Sb = createGAField("checkStorageTask"),
        Uc = createGAField("historyImportTask"),
        Tb = createGAField("samplerTask"),
        Vb = createGAFieldNoFunctions("_rlt"),
        Wb = createGAField("buildHitTask"),
        Xb = createGAField("sendHitTask"),
        Vc = createGAField("ceTask"),
        zd = createGAField("devIdTask"),
        Cd = createGAField("timingTask"),
        V = createGAFieldNoFunctions("name"),
        Q = createGAFieldNoFunctions("clientId", "cid"),
        Ad = createGAField("userId", "uid"),
        Na = createGAFieldNoFunctions("trackingId", "tid"),
        U = createGAFieldNoFunctions("cookieName", void 0, "_ga"),
        W = createGAFieldNoFunctions("cookieDomain"),
        Yb = createGAFieldNoFunctions("cookiePath", void 0, "/"),
        Zb = createGAFieldNoFunctions("cookieExpires", void 0, 63072E3),
        $b = createGAFieldNoFunctions("legacyCookieDomain"),
        Wc = createGAFieldNoFunctions("legacyHistoryImport", void 0, !0),
        ac = createGAFieldNoFunctions("storage", void 0, "cookie"),
        bc = createGAFieldNoFunctions("allowLinker", void 0, !1),
        cc = createGAFieldNoFunctions("allowAnchor", void 0, !0),
        Ka = createGAFieldNoFunctions("sampleRate", "sf", 100),
        dc = createGAFieldNoFunctions("siteSpeedSampleRate", void 0, 1),
        ec = createGAFieldNoFunctions("alwaysSendReferrer", void 0, !1);

    function ak_setupGAMethod(methodName, obj, method, methodId) { //function X(a, b, c, d) {
        obj[methodName] = function () {
            try {
                return methodId && ak_setFeatureUsed(methodId), method.apply(this, arguments);
            } catch (b) {
                var g = b && b.name;
                if (!(1 <= 100 * Math.random() || xa("?"))) {
                    var ca = ["t=error", "_e=exc", "_v=j23", "sr=1"];
                    methodName && ca.push("_f=" + methodName);
                    g && ca.push("_m=" + ak_encodeURIComponent(g.substring(0, 100)));
                    ca.push("aip=1");
                    ca.push("z=" + ak_random());
                    GaSendData(ca.join("&"));
                }
                throw b;
            }
        };
    }

    var Ed = function (a) {
        var b = Dd;
        if (b.fa && b.$) return 0;
        b.$ = !0;
        if (0 == b.V) return 0;
        void 0 === a && (a = ak_getRandom());
        return 0 == a % b.V ? Math.floor(a / b.V) % b.ea + 1 : 0;
    };

    function ak_flashVersion() { //function fc() {
        var a, b, c;
        if ((c = (c = window.navigator) ? c.plugins : null) && c.length)
            for (var d = 0; d < c.length && !b; d++) {
                var e = c[d]; - 1 < e.name.indexOf("Shockwave Flash") && (b = e.description);
            }
        if (!b) try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7"), b = a.GetVariable("$version");
        } catch (g) {}

        if (!b) try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6"), b = "WIN 6,0,21,0", a.AllowScriptAccess = "always", b = a.GetVariable("$version");
        } catch (ca) {}

        if (!b) try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash"), b = a.GetVariable("$version");
        } catch (l) {}

        b &&
            (a = b.match(/[\d]+/g)) && 3 <= a.length && (b = a[0] + "." + a[1] + " r" + a[2]);
        return b || void 0;
    }

    var gc = function (a, b) {
            var c = Math.min(getNumericValue(a, dc), 100);
            if (!(La(getStringValue(a, Q)) % 100 >= c) && (c = {}, Ec(c) || Fc(c))) {
                var d = c[Eb];
                void 0 == d || Infinity == d || isNaN(d) || (0 < d ? (Y(c, Gb), Y(c, Jb), Y(c, Ib), Y(c, Fb), Y(c, Hb), Y(c, Kb), Y(c, Lb), b(c)) : ak_addEvent(window, "load", function () {
                    gc(a, b);
                }, !1))
            }
        },
        Ec = function (a) {
            var b = window.performance || window.webkitPerformance,
                b = b && b.timing;

            if (!b) return !1;

            var c = b.navigationStart;

            if (0 == c) return !1;

            a[Eb] = b.loadEventStart - c;
            a[Gb] = b.domainLookupEnd - b.domainLookupStart;
            a[Jb] = b.connectEnd - b.connectStart;
            a[Ib] = b.responseStart - b.requestStart;
            a[Fb] = b.responseEnd - b.responseStart;
            a[Hb] = b.fetchStart - c;
            a[Kb] = b.domInteractive - c;
            a[Lb] = b.domContentLoadedEventStart - c;
            return !0;
        },
        Fc = function (a) {
            if (window.top != window) return !1;
            var b = window.external,
                c = b && b.onloadT;
            b && !b.isValidLoadTime && (c = void 0);
            2147483648 < c && (c = void 0);
            0 < c && b.setPageReadyTime();
            if (void 0 == c) return !1;
            a[Eb] = c;
            return !0;
        },
        Y = function (a, b) {
            var c = a[b];
            if (isNaN(c) || Infinity == c || 0 > c) a[b] = void 0;
        },
        /**
         * Automatically generates a site speed timing beacon based on the siteSpeedSampleRate setting for this tracker.
         * @param a
         * @returns {Function}
         */
        timingTask = function (a) { //Fd = function (a) {
            return function (b) {
                "pageview" != b.get(Va) || a.I || (a.I = !0, gc(b, function (b) {
                    a.send("timing", b);
                }))
            };
        };
    var cookieEnabled = !1, //hc = !1,
        ak_writeGACookie = function (a) {  //mc = function (a) {
            // https://developers.google.com/analytics/devguides/collection/analyticsjs/domains
            if ("cookie" == getStringValue(a, ac)) {
                var cookieName = getStringValue(a, U), // cookieName replaced variable b; U = "cookieName"
                    c = genGACookieValue(a),
                    d = kc(getStringValue(a, Yb)),    // cookiePath
                    e = lc(getStringValue(a, W)),     // cookieDomain
                    expires = 1E3 * getNumericValue(a, Zb),// expires replaced g
                    trackingId = getStringValue(a, Na);       // ca = getString...

                // automatic cookie configuration?
                if ("auto" != e) writeCookie(cookieName, c, d, e, trackingId, expires) && (cookieEnabled = !0);
                else {
                    ak_setFeatureUsed(32);
                    var l;
                    t: {
                        c = [];
                        e = ak_getHostname().split(".");
                        if (4 == e.length && (l = e[e.length - 1], parseInt(l, 10) == l)) {
                            l = ["none"];
                            break t;
                        }
                        for (l = e.length - 2; 0 <= l; l--) c.push(e.slice(l).join("."));
                        c.push("none");
                        l = c;
                    }
                    for (var k = 0; k < l.length; k++)
                        if (e = l[k], a.data.set(W, e), c = genGACookieValue(a), writeCookie(cookieName, c, d, e, trackingId, expires)) {
                            1 == ic(e) && ak_setFeatureUsed(36);
                            "none" == e && ak_setFeatureUsed(37);
                            cookieEnabled = !0;
                            return;
                        }
                    a.data.set(W, "auto");
                }
            }
        },
        /**
         * Aborts the request if the tracker is configured to use cookies but the user's browser has cookies disabled.
         * @param a
         */
        checkStorageTask = function (a) { //nc = function (a) {
            if ("cookie" == getStringValue(a, ac) &&
                !cookieEnabled && (ak_writeGACookie(a), !cookieEnabled)) throw "abort";
        },
        historyImportTask = function (a) { //Yc = function (a) {
            if (a.get(Wc)) {
                // __utma, see https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage
                // http://www.morevisibility.com/blogs/analytics/from-__utma-to-__utmz-google-analytics-cookies.html

                var b = getStringValue(a, W),
                    c = getStringValue(a, $b) || ak_getHostname(),
                    d = Xc("__utma", c, b);

                d && (ak_setFeatureUsed(19), a.set(Tc, (new Date).getTime(), !0), a.set(Rc, d.R), (b = Xc("__utmz", c, b)) && d.hash == b.hash && a.set(Sc, b.R))
            }
        },
        genGACookieValue = function (a) {  // nd = function (a) {
            var b = ak_encodeParens(getStringValue(a, Q)),   //encode clientId
                c = ic(getStringValue(a, W));   // cookieDomain
            a = jc(getStringValue(a, Yb));  // cookiePath
            1 < a && (c += "-" + a);
            return ["GA1", c, b].join(".");
        },
        Gc = function (a, b, c) {
            for (var d = [], e = [], g, ca = 0; ca < a.length; ca++) {
                var l = a[ca];
                if (l.r[c] == b) d.push(l);
                else void 0 == g || l.r[c] < g ? (e = [l], g = l.r[c]) : l.r[c] == g && e.push(l)
            }
            return 0 < d.length ? d : e;
        },
        lc = function (a) {
            return 0 ==
                a.indexOf(".") ? a.substr(1) : a;
        },
        ic = function (a) {
            return lc(a).split(".").length;
        },
        kc = function (a) {
            if (!a) return "/";
            1 < a.length && a.lastIndexOf("/") == a.length - 1 && (a = a.substr(0, a.length - 1));
            0 != a.indexOf("/") && (a = "/" + a);
            return a;
        },
        jc = function (a) {
            a = kc(a);
            return "/" == a ? 1 : a.split("/").length;
        };

    function Xc(a, b, c) {
        "none" == b && (b = "");
        var d = [],
            e = ak_getCookies(a);
        a = "__utma" == a ? 6 : 2;
        for (var g = 0; g < e.length; g++) {
            var ca = ("" + e[g]).split(".");
            ca.length >= a && d.push({
                hash: ca[0],
                R: e[g],
                O: ca
            })
        }
        return 0 == d.length ? void 0 : 1 == d.length ? d[0] : Zc(b, d) || Zc(c, d) || Zc(null, d) || d[0];
    }

    function Zc(a, b) {
        var c, d;
        null == a ? c = d = 1 : (c = La(a), d = La(ak_indexOf(a, ".") ? a.substring(1) : "." + a));
        for (var e = 0; e < b.length; e++)
            if (b[e].hash == c || b[e].hash == d) return b[e];
    }

    var regexUrlHost = new RegExp(/^https?:\/\/([^\/:]+)/),  // var od = new RegExp(/^https?:\/\/([^\/:]+)/),
        pd = /(.*)([?&#])(?:_ga=[^&#]*)(?:&?)(.*)/;

    function Bc(a) {
        a = a.get(Q); // get clientId
        var b = Ic(a, 0);
        return "_ga=1." + ak_encodeURIComponent(b + "." + a);
    }

    function Ic(a, b) {
        for (var c = new Date, d = window.navigator, e = d.plugins || [], c = [a, d.userAgent, c.getTimezoneOffset(), c.getYear(), c.getDate(), c.getHours(), c.getMinutes() + b], d = 0; d < e.length; ++d) c.push(e[d].description);
        return La(c.join("."));
    }
    var Dc = function (a) {
        ak_setFeatureUsed(48);
        this.target = a;
        this.T = !1;
    };

    Dc.prototype.Q = function (a, b) {
        if (a.tagName) {
            if ("a" == a.tagName.toLowerCase()) {
                a.href && ak_setHrefAttr(a, qd(this, a.href, b));
                return;
            }
            if ("form" == a.tagName.toLowerCase()) return rd(this, a);
        }
        if ("string" == typeof a) return qd(this, a, b);
    };

    var qd = function (a, b, c) {
            var d = pd.exec(b);
            d && 3 <= d.length && (b = d[1] + (d[3] ? d[2] + d[3] : ""));
            a = a.target.get("linkerParam");
            var e = b.indexOf("?"),
                d = b.indexOf("#");
            c ? b += (-1 == d ? "#" : "&") + a : (c = -1 == e ? "?" : "&", b = -1 == d ? b + (c + a) : b.substring(0, d) + c + a + b.substring(d));
            return b;
        },
        rd = function (a, b) {
            if (b && b.action) {
                var c = a.target.get("linkerParam").split("=")[1];
                if ("get" == b.method.toLowerCase()) {
                    for (var d = b.childNodes || [], e = 0; e < d.length; e++)
                        if ("_ga" == d[e].name) {
                            d[e].setAttribute("value", c);
                            return;
                        }
                    d = document.createElement("input");
                    d.setAttribute("type", "hidden");
                    d.setAttribute("name", "_ga");
                    d.setAttribute("value", c);
                    b.appendChild(d)
                } else "post" ==
                    b.method.toLowerCase() && (b.action = qd(a, b.action));
            }
        };

    Dc.prototype.S = function (a, b, c) {

        function a_area_tagsListener(c) {  //function d(c) {
            try {
                c = c || window.event;
                var d;
                t: {
                    var g = c.target || c.srcElement;
                    for (c = 100; g && 0 < c;) {
                        if (g.href && g.nodeName.match(/^a(?:rea)?$/i)) {
                            d = g;
                            break t
                        }
                        g = g.parentNode;
                        c--;
                    }
                    d = {}
                }
                ("http:" == d.protocol || "https:" == d.protocol) && sd(a, d.hostname || "") && d.href && ak_setHrefAttr(d, qd(self, d.href, b));
            } catch (w) {
                ak_setFeatureUsed(26);
            }
        }

        var self = this;  //var e = this;
        this.T || (this.T = !0, ak_addEvent(document, "mousedown", a_area_tagsListener, !1), ak_addEvent(document, "touchstart", a_area_tagsListener, !1), ak_addEvent(document, "keyup", a_area_tagsListener, !1));

        if (c) {
            c = function (b) {
                b = b || window.event;
                if ((b = b.target || b.srcElement) && b.action) {
                    var c = b.action.match(regexUrlHost);
                    c && sd(a, c[1]) && rd(self, b); // sd called with url's hostame in c[1]
                }
            };
            for (var g = 0; g < document.forms.length; g++) ak_addEvent(document.forms[g], "submit", c);
        }
    };

    function sd(a, hostname) { //function sd(a, b) {
        if (hostname == document.location.hostname) return !1;
        for (var c = 0; c < a.length; c++)
            if (a[c] instanceof RegExp) {
                if (a[c].test(hostname)) return !0;
            } else if (0 <= hostname.indexOf(a[c])) return !0;
        return !1;
    }

    var Jd = function (a, b, c) {
            var d = this;
            this.U = ed;
            this.aa = b;
            this.Y = c || Hd(a);
            var e = a.get(Wb);
            a.set(Wb, function (a) {
                a.get(d.U) || ("1" == ak_getCookies(d.Y)[0] ? a.set(d.U, "", !0) : a.set(d.U, "" + ak_random(), !0));
                var b = e(a);
                a.get(d.U) && writeCookie(d.Y, "1", a.get(Yb), a.get(W), a.get(Na), 6E5); // 10-minute expiration
                return b;
            });
            var g = a.get(Xb);
            a.set(Xb, function (a) {
                var b = g(a);
                Id(d, a);
                return b;
            })
        },
        Id = function (a, b) {
            if (b.get(a.U)) {
                var c = new GADict,
                    d = function (a) {
                        c.set(ak_getGAField(a).p, b.get(a));
                    };
                d(hb);
                d(ib);
                d(Na);
                d(Q);
                d(a.U);
                d(ld);
                var url = a.aa;
                "/" === e.charAt(0) && (e = getProtocol() + e);
                c.map(function (k, v) {
                    url +=
                        ak_encodeURIComponent(k) + "=" + ak_encodeURIComponent("" + v) + "&"
                });
                url += "z=" + ak_random();
                ak_fireImageBeacon(url);
                b.set(a.U, "", !0);
            }
        },
        Hd = function (a) {
            return a.get(V) && "t0" != a.get(V) ? "_dc_" + ak_encodeParens(a.get(V)) : "_dc";
        };
    var Dd, Gd;
    Dd = new function () {
        this.V = 10;
        this.fa = void 0;
        this.$ = !1;
        this.ea = 1;
    };

    Gd = !1;
    var fd = function (a, b) {
        var c = a.b;
        if (!c.get("dcLoaded")) {
            ak_setFeatureUsed(29);
            b = b || {};
            var d;
            b[U] && (d = ak_encodeParens(b[U]));
            var e = "//stats.g.doubleclick.net/collect?t=dc&aip=1&";
            if ("https:" != document.location.protocol && !forceSSL) {
                var g = La(c.get(Q));
                if (Gd || Ed(g)) e = "https:" + e, ak_setFeatureUsed(33), Gd = !0;
            }
            new Jd(c, e, d);
            c.set("dcLoaded", !0);
        }
    };

    var Kd = function () {
        ak_setFeatureUsed(38);
    };

    function getAdSenseId() { //$c() {
        var a = window.gaGlobal = window.gaGlobal || {};
        return a.hid = a.hid || ak_random();
    }

    var ad, bd = function (a, b, c) {
        if (!ad) {
            var d;
            d = document.location.hash;
            var e = window.name,
                g = /^#?gaso=([^&]*)/;

            if (e = (d = (d = d && d.match(g) || e && e.match(g)) ? d[1] : ak_getCookies("GASO")[0] || "")
                && d.match(/^(?:!([-0-9a-z.]{1,40})!)?([-.\w]{10,1200})$/i)) {
                writeCookie("GASO", "" + d, c, b, a, 0),
                window._udo || (window._udo = b),
                window._utcp || (window._utcp = c),
                a = e[1],
                ak_loadScript("https://www.google.com/analytics/web/inpage/pub/inpage.js?" + (a ? "prefix=" + a + "&" : "") + ak_random(), "_gasojs");
            }

            ad = !0;
        }
    };

    var isTrackingIdValid = /^(UA|YT|MO|GP)-(\d+)-(\d+)$/,  //wb = /^(UA|YT|MO|GP)-(\d+)-(\d+)$/,
        Tracker = function (a) {  //pc = function (a) {

            function setField(name, fieldObj) {
                self.b.data.set(name, fieldObj);
            }

            function addFilter(a, c) {
                setField(a, c);
                self.filters.add(a)
            }


            var self = this; //var d = this;

            this.b = new Ya;
            this.filters = new GAFilters();  // this.filters = new Ha;

            setField(V, a[V]);
            setField(Na, ak_trim(a[Na]));  // tracking Id
            setField(U, a[U]);
            setField(W, a[W] || ak_getHostname());
            setField(Yb, a[Yb]);
            setField(Zb, a[Zb]);
            setField($b, a[$b]);
            setField(Wc, a[Wc]);
            setField(bc, a[bc]);
            setField(cc, a[cc]);
            setField(Ka, a[Ka]);
            setField(dc, a[dc]);
            setField(ec, a[ec]);
            setField(ac, a[ac]);
            setField(Ad, a[Ad]);
            setField(hb, 1);
            setField(ib, "j23");
            addFilter(Qb, _oot);
            addFilter(dd, previewTask);
            addFilter(Rb, checkProtocolTask);
            addFilter(md, validationTask);
            addFilter(Sb, checkStorageTask);
            addFilter(Uc, historyImportTask);
            addFilter(Tb, samplerTask);
            addFilter(Vb, _rlt);
            addFilter(Vc, ceTask);
            addFilter(zd, devIdTask);
            addFilter(Wb, buildHitTask);
            addFilter(Xb, sendHitTask);
            addFilter(Cd, timingTask(this));
            Jc(this.b, a[Q]);  // clientId
            setBrowserFields(this.b);  // Kc(this.b);
            this.b.set(jb, getAdSenseId());
            bd(this.b.get(Na), this.b.get(W), this.b.get(Yb))
        },
        Jc = function (a, b) {
            if ("cookie" == getStringValue(a, ac)) {
                cookieEnabled = !1;
                var c;
                i: {
                    var d = ak_getCookies(getStringValue(a, U));  // U "cookieName"
                    if (d && !(1 > d.length)) {
                        c = [];

                        for (var e = 0; e < d.length; e++) {
                            var g;
                            g = d[e].split(".");
                            var ca = g.shift();
                            ("GA1" == ca || "1" == ca) && 1 < g.length ?
                                ( ca = g.shift().split("-"),
                                  1 == ca.length && (ca[1] = "1"), ca[0] *= 1, ca[1] *= 1,
                                  g = {
                                    r: ca,
                                    s: g.join(".")
                                  }) : g = void 0;
                            g && c.push(g);
                        }

                        if (1 == c.length) {
                            ak_setFeatureUsed(13);
                            c = c[0].s;
                            break i;
                        }

                        if (0 == c.length) ak_setFeatureUsed(12);
                        else {
                            ak_setFeatureUsed(14);
                            d = ic(getStringValue(a, W));
                            c = Gc(c, d, 0);
                            if (1 == c.length) {
                                c = c[0].s;
                                break i;
                            }
                            d = jc(getStringValue(a, Yb));
                            c = Gc(c, d, 1);
                            c = c[0] && c[0].s;
                            break i;
                        }
                    }
                    c = void 0;
                }
                c || (c = getStringValue(a, W), d = getStringValue(a, $b) || ak_getHostname(), c = Xc("__utma", d, c), (c = void 0 == c ? void 0 : c.O[1] + "." + c.O[2]) && ak_setFeatureUsed(10));
                c && (a.data.set(Q, c), cookieEnabled = !0);
            }
            c = a.get(cc);  // allowAnchor flag

            // https://developer.mozilla.org/en-US/docs/Web/API/URLUtils.search
            if (e = (c = document.location[c ? "href" : "search"].match(
                    "(?:&|#|\\?)" + ak_encodeURIComponent("_ga").replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1") + "=([^&#]*)"))
                && 2 == c.length ? c[1] : "") {
                a.get(bc) ? (c = e.indexOf("."), -1 == c ? ak_setFeatureUsed(22) : (d = e.substring(c + 1), "1" != e.substring(0, c) ? ak_setFeatureUsed(22) : (c = d.indexOf("."), -1 == c ? ak_setFeatureUsed(22) : (e = d.substring(0, c), c = d.substring(c + 1), e != Ic(c, 0) && e != Ic(c, -1) && e != Ic(c, -2) ? ak_setFeatureUsed(23) : (ak_setFeatureUsed(11), a.data.set(Q, c)))))) : ak_setFeatureUsed(21);
            }

            b && (ak_setFeatureUsed(9), a.data.set(Q, ak_encodeURIComponent(b)));
            a.get(Q) || ((c = (c = window.gaGlobal && window.gaGlobal.vid) && -1 != c.search(/^(?:utma\.)?\d+\.\d+$/) ? c : void 0) ? (ak_setFeatureUsed(17), a.data.set(Q, c)) : (ak_setFeatureUsed(8), a.data.set(Q, [ak_random() ^ Mc() & 2147483647, Math.round((new Date).getTime() / 1E3)].join("."))));
            ak_writeGACookie(a);
        },
        setBrowserFields = function (a) { //Kc = function (a) {
            var b = window.navigator,
                c = window.screen,
                d = document.location;
            a.set(lb, ak_getReferrer(a.get(ec)));
            if (d) {
                var e = d.pathname || "";
                "/" != e.charAt(0) && (ak_setFeatureUsed(31), e = "/" + e);
                a.set(kb, d.protocol + "//" + d.hostname + e + d.search)
            }
            c && a.set(qb, c.width + "x" + c.height);
            c && a.set(pb, c.colorDepth + "-bit");
            var c = document.documentElement,
                g = (e = document.body) && e.clientWidth &&
                e.clientHeight,
                ca = [];
            c && c.clientWidth && c.clientHeight && ("CSS1Compat" === document.compatMode || !g) ? ca = [c.clientWidth, c.clientHeight] : g && (ca = [e.clientWidth, e.clientHeight]);
            c = 0 >= ca[0] || 0 >= ca[1] ? "" : ca.join("x");
            a.set(rb, c);
            a.set(tb, ak_flashVersion());
            a.set(ob, document.characterSet || document.charset);
            a.set(sb, b && "function" === typeof b.javaEnabled && b.javaEnabled() || !1);
            a.set(nb, (b && (b.language || b.browserLanguage) || "").toLowerCase());
            if (d && a.get(cc) && (b = document.location.hash)) {
                b = b.split(/[?&#]+/);
                d = [];
                for (c = 0; c < b.length; ++c)(ak_indexOf(b[c], "utm_id") || ak_indexOf(b[c], "utm_campaign") || ak_indexOf(b[c], "utm_source") || ak_indexOf(b[c], "utm_medium") || ak_indexOf(b[c], "utm_term") ||
                    ak_indexOf(b[c], "utm_content") || ak_indexOf(b[c], "gclid") || ak_indexOf(b[c], "dclid") || ak_indexOf(b[c], "gclsrc")) && d.push(b[c]);
                0 < d.length && (b = "#" + d.join("&"), a.set(kb, a.get(kb) + b))
            }
        };

    Tracker.prototype.get = function (a) {
        return this.b.get(a);
    };

    Tracker.prototype.set = function (a, b) {
        this.b.set(a, b);
    };

    var qc = {
        pageview: [mb],
        event: [ub, xb, yb, zb],
        social: [Bb, Cb, Db],
        timing: [Mb, Nb, Pb, Ob]
    };

    Tracker.prototype.send = function (a) {
        if (!(1 > arguments.length)) {
            var b, c;
            "string" === typeof arguments[0] ? (b = arguments[0], c = [].slice.call(arguments, 1)) : (b = arguments[0] && arguments[0][Va], c = arguments);
            b && (c = wa(qc[b] || [], c), c[Va] = b, this.b.set(c, void 0, !0), this.filters.D(this.b), this.b.data.m = {});
        }
    };

    var rc = function (a) {
        if ("prerender" == document.visibilityState) return !1;
        a();
        return !0;
    };

    var td = /^(?:(\w+)\.)?(?:(\w+):)?(\w+)$/,
        sc = function (a) {
            if (ak_isfunction(a[0])) this.u = a[0];
            else {
                var b = td.exec(a[0]);
                null != b && 4 == b.length && (this.c = b[1] || "t0", this.e = b[2] || "", this.d = b[3], this.a = [].slice.call(a, 1), this.e || (this.A = "create" == this.d, this.i = "require" == this.d, this.g = "provide" == this.d, this.ba = "remove" == this.d), this.i && (3 <= this.a.length ? (this.X = this.a[1], this.W = this.a[2]) : this.a[1] && (ak_isString(this.a[1]) ? this.X = this.a[1] : this.W = this.a[1])));
                b = a[1];
                a = a[2];
                if (!this.d) throw "abort";
                if (this.i && (!ak_isString(b) || "" == b)) throw "abort";
                if (this.g &&
                    (!ak_isString(b) || "" == b || !ak_isfunction(a))) throw "abort";
                if (ud(this.c) || ud(this.e)) throw "abort";
                if (this.g && "t0" != this.c) throw "abort";
            }
        };

    function ud(a) {
        return 0 <= a.indexOf(".") || 0 <= a.indexOf(":");
    }

    var Z = {
        F: "/plugins/ua/",
        H: "plugins_",
        ga: function () {
            Z.f = [];
            Z.k = new GADict;
            Z.Z = new GADict;
            Z.ca = {
                ec: 45,
                ecommerce: 46,
                linkid: 47
            }
        }
    };

    Z.ga();

    Z.B = function (a, b, c) {
        b == $ ? ak_setFeatureUsed(35) : b.get(V);
        var d = Z.k.get(a);

        if (!ak_isfunction(d)) return !1;

        b[Z.H] = b[Z.H] || new GADict;

        if (b[Z.H].get(a)) return !0;

        b[Z.H].set(a, new d(b, c || {}));
        return !0;
    };

    Z.da = function (a) {
        var b = a.a[0];

        if (!ak_isfunction(Z.k.get(b)) && !Z.Z.get(b) &&
            (Z.ca.hasOwnProperty(b) && ak_setFeatureUsed(Z.ca[b]), a = a.X, !a && Z.ca.hasOwnProperty(b) ? (ak_setFeatureUsed(39), a = b + ".js") : ak_setFeatureUsed(43), a)) {

            a && 0 <= a.indexOf("/") || (a = getProtocol() + "//www.google-analytics.com" + Z.F + a);
            a = tc(a);
            var c = a.protocol,
                d = document.location.protocol;

            if (c = "https:" == c || c == d ? !0 : "http:" != c ? !1 : "http:" == d) {
                c = tc(document.location.href), a.G || 0 <= a.url.indexOf("?") || 0 <= a.path.indexOf("://") ? c = !1 : a.host == c.host && a.port == c.port ? c = !0 : (c = "http:" == a.protocol ? 80 : 443, c = "www.google-analytics.com" == a.host && (a.port || c) == c && ak_indexOf(a.path, "/plugins/") ? !0 : !1);
            }

            c && (ak_loadScript(a.url), Z.Z.set(b, !0));
        }
    };

    Z.C = function (a, b) {
        Z.k.set(a, b);
    };

    Z.D = function (a) {
        var b = Z.J.apply(Z, arguments),
            b = Z.f.concat(b);

        for (Z.f = []; 0 < b.length && !Z.v(b[0]) && !(b.shift(), 0 < Z.f.length); );

        Z.f = Z.f.concat(b);
    };

    Z.J = function (a) {
        for (var b = [], c = 0; c < arguments.length; c++) try {
            var d = new sc(arguments[c]);
            d.g ? Z.C(d.a[0], d.a[1]) : (d.i && Z.da(d), b.push(d));
        } catch (e) {}

        return b;
    };

    Z.v = function (a) {
        try {
            if (a.u) a.u.call(window, $.getTracker("t0"));
            else {
                var b = a.c == gb ? $ : $.getTracker(a.c);
                if (a.A) "t0" == a.c && $.create.apply($, a.a);
                else if (a.ba) $.remove(a.c);
                else if (b)
                    if (a.i) {
                        if (!Z.B(a.a[0], b, a.W)) return !0;
                    } else a.e && (b = b[Z.H].get(a.e)), b[a.d].apply(b, a.a);
            }
        } catch (c) {}
    };

    function tc(a) {

        function b(a) {
            var b = (a.hostname || "").split(":")[0].toLowerCase(),
                c = (a.protocol || "").toLowerCase(),
                c = 1 * a.port || ("http:" == c ? 80 : "https:" == c ? 443 : "");
            a = a.pathname || "";
            ak_indexOf(a, "/") || (a = "/" + a);
            return [b, "" + c, a]
        }

        var c = document.createElement("a");
        ak_setHrefAttr(c, document.location.href);

        var d = (c.protocol || "").toLowerCase(),
            e = b(c),
            g = c.search || "",
            ca = d + "//" + e[0] + (e[1] ? ":" + e[1] : "");

        ak_indexOf(a, "//") ? a = d + a : ak_indexOf(a, "/") ? a = ca + a : !a || ak_indexOf(a, "?") ? a = ca + e[2] + (a || g) : 0 > a.split("/")[0].indexOf(":") && (a = ca + e[2].substring(0, e[2].lastIndexOf("/")) + "/" + a);
        ak_setHrefAttr(c, a);
        d = b(c);

        return {
            protocol: (c.protocol || "").toLowerCase(),
            host: d[0],
            port: d[1],
            path: d[2],
            G: c.search ||
                "",
            url: a || ""
        };
    }

    var $ = function (a) {
        ak_setFeatureUsed(1);
        Z.D.apply(Z, [arguments]);
    };

    $.trackersDict = {};  //$.h = {};

    $.trackersList = [];  //$.P = [];

    $.L = 0;
    $.answer = 42;   // number of fields in the tracker object, or http://en.wikipedia.org/wiki/42_(number)#The_Hitchhiker.27s_Guide_to_the_Galaxy
    var uc = [Na, W, V]; // "trackingId", "cookieDomain", "name"

    $.create = function (a) {
        var b = wa(uc, [].slice.call(arguments));
        b[V] || (b[V] = "t0"); // default tracker object name is 't0'
        var c = "" + b[V];
        if ($.trackersDict[c]) return $.trackersDict[c];
        b = new Tracker(b);
        $.trackersDict[c] = b;
        $.trackersList.push(b);
        return b;
    };

    $.remove = function (a) {
        for (var b = 0; b < $.trackersList.length; b++) {
            if ($.trackersList[b].get(V) == a) {
                $.trackersList.splice(b, 1);
                $.trackersDict[a] = null;
                break;
            }
        }
    };

    $.getTracker = function (name) {
        return $.trackersDict[name];
    };

    $.K = function () {
        return $.trackersList.slice(0);
    };

    $.N = function () {
        "ga" != gb && ak_setFeatureUsed(49);
        var a = window[gb];

        if (!a || 42 != a.answer) {
            $.L = a && a.l;    // execution time of script tag www.google-analytics.com/analytics.js
            $.loaded = !0;
            var b = window[gb] = $;
            ak_setupGAMethod("create", b, b.create, 3);
            ak_setupGAMethod("remove", b, b.remove);
            ak_setupGAMethod("getByName", b, b.j, 5);
            ak_setupGAMethod("getAll", b, b.K, 6);
            b = Tracker.prototype;
            ak_setupGAMethod("get", b, b.get, 7);
            ak_setupGAMethod("set", b, b.set, 4);
            ak_setupGAMethod("send", b, b.send, 2);
            b = Ya.prototype;
            ak_setupGAMethod("get", b, b.get);
            ak_setupGAMethod("set", b, b.set);
            (window.gaplugins = window.gaplugins || {}).Linker = Dc;
            b = Dc.prototype;
            Z.C("linker", Dc);
            ak_setupGAMethod("decorate", b, b.Q, 20);
            ak_setupGAMethod("autoLink", b, b.S, 25);
            Z.C("displayfeatures", fd);   // doubleclick.net-related
            Z.C("adfeatures", Kd);
            a = a && a.q;
            ak_isArray(a) ? Z.D.apply($, a) : ak_setFeatureUsed(50);
        }
    };

    (function () {
        var a = $.N;
        if (!rc(a)) {
            ak_setFeatureUsed(16);
            var b = !1,
                c = function () {
                    !b && rc(a) && (b = !0, ak_removeEvent(document, "visibilitychange", c))
                };
            ak_addEvent(document, "visibilitychange", c);
        }
    })();

    function La(a) {
        var b = 1,
            c = 0,
            d;
        if (a)
            for (b = 0, d = a.length - 1; 0 <= d; d--) c = a.charCodeAt(d), b = (b << 6 & 268435455) + c + (c << 14), c = b & 266338304, b = 0 != c ? b ^ c >> 21 : b;
        return b;
    }
})(window);
