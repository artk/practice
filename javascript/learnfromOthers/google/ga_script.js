// Consult https://developers.google.com/analytics/devguides/collection/analyticsjs/advanced


// script provided by Google for loading analytics script
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-XXXXXXXX-Y', 'auto', {'cookieDomain' : 'none'});  // cookieDomain set to none is for localhost testing
  ga('send', 'pageview');
  ga(function() { alert("finished loading ga");}); // sample code from devguide's Pushing functions section


/**
 *  NOTES
 *
(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;                // window['GoogleAnalyticsObject']='ga';
    i[r] = i[r] || function() {                    // window['ga'] = window['ga'] || function() {
        (i[r].q = i[r].q || []).push(arguments)    // (window['ga'].q = window['ga'].q || []).push(arguments)
    }, i[r].l = 1 * new Date();                    // window['ga'].l = 1 * new Date();  ga object creation timestamp
    a = s.createElement(o),                        // a = document.createElement('script'),
    m = s.getElementsByTagName(o)[0];              // m = document.getElementsByTagName('script')[0];
    a.async = 1;
    a.src = g;                                     // a.src = '//www.google-analytics.com/analytics.js'
    m.parentNode.insertBefore(a, m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

*
*  ga function stores instructions/functions in a queue (ga function's static member q). These instructions/functions
*  are executed when analytics.js is loaded
*
*
**/

