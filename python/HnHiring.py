__author__ = 'art'

from urllib import parse
from bs4 import element
from WebClient import WebClient

"""
Class to read and filter Hacker News's Monthly 'Ask HN : Who is Hiring' pages
"""
class HnHiring:
    def __init__(self, title=None):
        self.urlcomponents = None
        self.filters = []
        self.wc = WebClient()
        return

    def addfilter(self, fn):
        if hasattr(fn, '__call__'):
            self.filters.append(fn)

    def filterhtmldoc(self, bs, jobsindex=1):
        root_table = bs.find("table")
        job_section = None

        for i, child in enumerate(root_table.children):
            if i == 2:  # third <tr> contains the jobs
                job_section = child
                break

        if job_section:
            jobs = job_section.find_all("table")[jobsindex]

            for job in jobs.children:
                job_table = job.find('table')
                if job.table:
                    sc = job.select("span.comment > font")
                    if sc:
                        for elem in sc[0].contents:  # job description text
                            if type(elem) == element.NavigableString:
                                passedfilters = False
                                for filterfn in self.filters:
                                    passedfilters = filterfn(elem)
                                    if not passedfilters:
                                        break

                                if passedfilters:
                                    self.newhtmldoc.append(str(job_table))

                                break

                elif job.a and job.a['href'] and job.a.string == 'More':
                    # technically, this should not be done because of rel='nofollow', but this program was written to learn Beautiful Soup and will not be run daily
                    nextpage = "{}://{}{}".format(self.urlcomponents.scheme, self.urlcomponents.netloc, job.a['href'])
                    self.filterhtmldoc(self.wc.read(nextpage), 0)

    def htmldoc(self, url, subtitle):
        self.urlcomponents = parse.urlparse(url)
        self.newhtmldoc = ['<html>', '<head>', '<title>Filtered HN Jobs</title>', '<body>']
        bs = self.wc.read(url)
        self.newhtmldoc.append("<h1>{}</h1>".format(bs.title.get_text()))
        self.newhtmldoc.append("<h2>{}</h2>".format(subtitle))
        self.newhtmldoc.append("<h2><a href='{}'>Source: {}</a></h2>".format(url, url))
        self.filterhtmldoc(bs)
        self.newhtmldoc.extend(['</body>', '</html>'])
        return '\n'.join(self.newhtmldoc)
