__author__ = 'art'
from urllib.request import *
from bs4 import BeautifulSoup


class WebClient:
    """
            http://stackoverflow.com/questions/13303449/urllib2-httperror-http-error-403-forbidden
    """

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.77.4 (KHTML, like Gecko) Version/7.0 Safari/537.77.4',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'
    }

    def __init__(self, headers=None):
        if headers is not None:
            for k in headers.keys():
                self.headers[k] = headers[k]

    def read(self, url, data=None):
        req = Request(url, data, self.headers)
        return BeautifulSoup(urlopen(req).read())
