__author__ = 'art'

import sys
from HnHiring import *
from urllib.error import HTTPError
import re
import datetime

def usage(cmd):
    print("Usage: {} URL".format(cmd))

if len(sys.argv) < 2:
    usage(sys.argv[0])
    sys.exit(1)

try:
    citiesfilter = re.compile('Mountain View|Palo Alto|San Francisco|San Mateo|Redwood City|Santa Clara|San Jose|San Bruno|Oakland|Berkeley|Sunnyvale|Cupertino|Santa Clara', re.IGNORECASE)

    hnjobs = HnHiring()
    hnjobs.addfilter(lambda s: citiesfilter.search(s) is not None)
    doc = hnjobs.htmldoc(sys.argv[1], "Silicon Valley Jobs: {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
    print(doc)

except HTTPError as err:
    print("{}; {}\n{}\n{}".format(err.code, err.reason, err.msg, err.headers))



