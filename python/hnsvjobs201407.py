__author__ = 'art'
from HnHiring import *
from urllib.error import HTTPError
import re
import datetime

# test program for HnHiring, filter July 2014 jobs by Silicon Valley cities
try:
    url = 'https://news.ycombinator.com/item?id=7970366'
    citiesfilter = re.compile('Mountain View|Palo Alto|San Francisco|San Mateo|Redwood City|Santa Clara|San Jose|San Bruno|Oakland|Berkeley|Sunnyvale|Cupertino|Santa Clara', re.IGNORECASE)

    hnjobs = HnHiring()
    hnjobs.addfilter(lambda s: citiesfilter.search(s) is not None)
    doc = hnjobs.htmldoc(url, "Silicon Valley Jobs: {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
    print(doc)

except HTTPError as err:
    print("{}; {}\n{}\n{}".format(err.code, err.reason, err.msg, err.headers))

