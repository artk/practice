# README #

##Coding Practice Repository ##
 
### Repository Uses: ###

* Write small programs in different languages.
* Write programs for reviewing and studying data structures and algorithms
* Write programs for learning how to use frameworks (e.g., Mocha for testing JS code), APIs, etc.
* Hadoop Map/Reduce review/study
* SQL review/study
