#!/usr/bin/python

import sys

#
#  Reducer for Summarizaton Design Pattern - Inverted Index
#
#  Exercise 4.3a
#  Build an inverted index listing each word and the list of nodes that contain the word
#

nodeids = []
oldKey = None
oldNode = None

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    thisKey, thisNode = data_mapped

    if oldKey and oldKey != thisKey:
        nodeids.sort()
        print("{}\t{}".format(oldKey, nodeids))
        oldKey = thisKey
        nodeids = []

    oldKey = thisKey

    if oldNode is None or oldNode != thisNode:
        nodeids.append(int(thisNode))

    oldNode = thisNode

if oldKey is not None:
    nodeids.sort()
    print("{}\t{}".format(oldKey, nodeids))

