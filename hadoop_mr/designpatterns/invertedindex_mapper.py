#!/usr/bin/python
import sys
import csv
import re


#
#  Mapper for Summarization Design Pattern: Inverted Index
#
#  Exercise 4.3
#
#  Build an inverted index of words from the body field of udacity's forum post data
#  The mapper outputs a word and its node id.
#

def mapper():
    reader = csv.reader(sys.stdin, delimiter='\t')
    next(reader, None)

    for line in reader:
        nodeid = line[0]
        body = line[4]
        words = re.split("\s|\.|\?|!|:|;|,|\"|\(|\)|<|>|\[|]|#|$|=|-|/", body)

        for word in words:
            if word != '':
                print "{0}\t{1}".format(word.lower(), nodeid)


# This function allows you to test the mapper with the provided test string
def main():
    mapper()

main()
