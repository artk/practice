#!/usr/bin/python
import sys
import csv
import re

#
# Exercise 4.5:
#
# Mapper - Structural Pattern : Structure to Hierarchical
# input is forum_node.tsv or forum_user.tsv
#
#
# Combine two datasets by joining them by the user_id
# This will be used to answer the question: "What is the reputation of the post's author?"
#

def mapper_forum_users(line):
    """
    the mapper key should be "user_ptr_id" from "forum_users.tsv"
    users is Table A
    """
    return [line[0], "A"] + line[1:]


def mapper_forum_node(line):
    """
    "author_id" from "forum_nodes.tsv"
    posts is Table B
    """
    id, title, tagnames, author_id, body, node_type, parent_id, abs_parent_id, \
    added_at, score, state_string, last_edited_id, last_activity_by_id, \
    last_activity_at, active_revision_id, extra, extra_ref_id, \
    extra_count, marked = line

    return [author_id, "B",
            id, title, tagnames, author_id,
            body.encode('string_escape'),
            node_type, parent_id, abs_parent_id,
            added_at, score, state_string, last_edited_id, last_activity_by_id,
            last_activity_at, active_revision_id, extra, extra_ref_id,
            extra_count, marked]


def mapper():
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    regex = re.compile("^[0-9]+$")

    for line in reader:
        try:
            if len(line) != 5 and len(line) != 19:
                continue

            if regex.search(line[0]) is None:
                continue

            if len(line) == 5:
                writer.writerow(mapper_forum_users(line))
            else:
                writer.writerow(mapper_forum_node(line))

        except:
            pass

# This function allows you to test the mapper with the provided test string
def main():
    mapper()

main()