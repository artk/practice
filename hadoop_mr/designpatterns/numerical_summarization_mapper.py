#!/usr/bin/python
import sys
import datetime

#
#  Exercise 4.4
#
#  Mapper - Numerical Summarization Design Pattern
#
#  Calculate the mean sales per day of the week
#
#  Output weekday (iso, Sunday is 7), cost of sale, number of sales

numsales = 1
for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) == 6:
        date, time, store, item, cost, payment = data
        year, month, day = date.split("-")
        weekday = datetime.datetime(int(year), int(month), int(day)).isoweekday()
        print("{0}\t{1}\t{2}".format(weekday, cost, numsales))

