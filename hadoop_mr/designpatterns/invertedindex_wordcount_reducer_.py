#!/usr/bin/python

import sys

#
#  Reducer for Summarizaton Design Pattern - Inverted Index
#
#  Exercise 4.3a
#  Count the number of occurrences of each word in the Inverted Index
#


occurrences = 0
oldKey = None

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 2:
        continue

    thisKey, thisNode = data_mapped

    if oldKey and oldKey != thisKey:
        print("{}\t{}".format(oldKey, occurrences))
        oldKey = thisKey
        occurrences = 0

    oldKey = thisKey
    occurrences += 1

if oldKey is not None:
    print("{}\t{}".format(oldKey, occurrences))

