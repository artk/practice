#!/usr/bin/python
import sys
import csv

###########################################################################
#
# Exercise 4.2  Top N design pattern
#
# Find the top 10 longest forum post in the Udacity forum data.
#
# 5th field of line - length of forum post
#


def mapper():
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    topN = []
    N = 10

    for line in reader:
        i = 0
        while i < len(topN):
            rank = int(line[4])
            if rank < topN[i][0]:
                topN.insert(i, (rank, line))
                break
            i += 1

        if i == len(topN):
            topN.append((int(line[4]), line))

        if len(topN) > N:
            topN.pop(0)  # remove smallest element

    for line in topN:
        writer.writerow(line[1])


test_text = """\"\"\t\"\"\t\"\"\t\"\"\t\"333\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"88888888\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"1\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"11111111111\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"1000000000\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"22\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"4444\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"666666\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"55555\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"999999999\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"7777777\"\t\"\"
"""

# This function allows you to test the mapper with the provided test string
def main():
    import StringIO

    sys.stdin = StringIO.StringIO(test_text)
    mapper()
    sys.stdin = sys.__stdin__


main()