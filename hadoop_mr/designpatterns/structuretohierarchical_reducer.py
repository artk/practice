#!/usr/bin/python
import sys,csv

#
# Exercise 4.5:
#
# Reducer - Structural Pattern : Structure to Hierarchical
#
# Join tableA and tableB by using key (column 1)
#

oldKey = None
tableAKey = None
tableBKey = None

forum_node = []
userinfo = []

reader = csv.reader(sys.stdin, delimiter='\t')

for line in reader:
    thisKey = line[0]
    thisTable = line[1]

    if oldKey and oldKey != thisKey:
        tableAKey = None
        tableBKey = None

    oldKey = thisKey
    if thisTable is "A":
        tableAKey = thisKey
        userinfo = line[2:]
    elif thisTable is "B":
        tableBKey = thisKey
        forum_node = line[2:]

    if tableAKey == tableBKey:
        row = forum_node + userinfo
        print '\t'.join(row)

