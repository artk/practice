#!/usr/bin/python
import sys

#
#  Exercise 4.4
#
#  Reducer - Numerical Summarization Design Pattern
#
#  Calculate the mean sales (revenue) per day of the week
#
#  Output weekday (iso, Sunday is 7), mean sales, number of sales
#
#  This job can be executed with the combiner
#

salesTotal = 0
numSales = 0
oldKey = None

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 4:
        # Something has gone wrong. Skip this line.
        continue

    thisKey, thisSale, count = data_mapped

    if oldKey and oldKey != thisKey:
        print("{0}\t{1}\t1\t{2}".format(oldKey, round(salesTotal / numSales, 2), numSales))
        oldKey = thisKey
        salesTotal = 0
        numSales = 0

    oldKey = thisKey
    salesTotal += float(thisSale)
    numSales += int(count)

if oldKey is not None:
    print("{0}\t{1}\t1\t{2}".format(oldKey, round(salesTotal / numSales, 2), numSales))

