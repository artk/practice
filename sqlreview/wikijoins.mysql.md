#SQL Joins not covered by [Wikipedia's Join](http://en.wikipedia.org/wiki/Join_%28SQL%29)

##Employee - Department (Employee\Department)##

    select *
    from employee e left join department d
    on e.departmentid = d.departmentid
    where d.departmentID is null;


Employee.LastName | Employee.DepartmentID | Department.DepartmentID | Department.DepartmentName
----------------- | --------------------- | ----------------------- | -------------------------
John | NULL | NULL | NULL

##Department - Employee (Department\Employee)##
    select *
    from employee e right join department d
    on e.departmentid = d.departmentid
    where e.departmentID is null;

Employee.LastName | Employee.DepartmentID | Department.DepartmentID | Department.DepartmentName
----------------- | --------------------- | ----------------------- | -------------------------
NULL | NULL | 35 | Marketing


##Employee ∆ Department (Symmetric Difference)##
    select *
    from employee e left join department d
    on e.departmentid = d.departmentid
    where d.departmentID is null
    union
    select *
    from employee e right join department d
    on e.departmentid = d.departmentid
    where e.departmentID is null;

Employee.LastName | Employee.DepartmentID | Department.DepartmentID | Department.DepartmentName
----------------- | --------------------- | ----------------------- | -------------------------
John | NULL | NULL | NULL
NULL | NULL | 35 | Marketing


##MySQL version of Full Outer Join##
    select * from employee e left join department d on e.departmentid=d.departmentid
    union
    select * from employee e right join department d on e.departmentid=d.departmentid


