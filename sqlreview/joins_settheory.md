#JOINS and Set Theory#


##Inner Join [A ∩ B]##
select <list of columns>  from tableA A **inner join** tableB B on A.key=B.key

A ∩ B = {x | x ∈ A and x ∈ B}


##Full Outer Join [A ∪ B]##
select <list of columns> from tableA A **full outer join** tableB B on
A.key=B.key

A ∪ B = {x | x ∈ A or x ∈ B}

    MySQL

    select <list of columns> from tableA A left join tableB B on A.key=B.key
        union
    select <list of columns> from tableA A right join tableB B on A.key=B.key


##Left Join [A ∪ (A ∩ B)]##
select <list of columns> from tableA A **left join** tableB B on
A.key=B.key


##Right Join [B ∪ (A ∩ B)]##
select <list of columns> from tableA A **right join** tableB B on
A.key=B.key

##A - B, [A\B](https://www.khanacademy.org/math/probability/independent-dependent-probability/basic_set_operations/v/relative-complement-or-difference-between-sets) (relative complement of B in A)##
select <list of columns> from tableA A **left join** tableB B on
A.key=B.key where B.key is null;

A - B = {x | x ∈ A and x ∉ B}

    A = {31, 33, 34, 39}
    B = {31, 33, 34, 35}
    A - B = {39}

##B - A, B\A##
select <list of columns> from tableA A **right join** tableB B on
A.key=B.key where A.key is null;

B - A = {x | x ∈ B and x ∉ A}

    A = {31, 33, 34, 39}
    B = {31, 33, 34, 35}
    B - A = {35}

##[A ∆ B, A ⊖ B](http://en.wikipedia.org/wiki/Symmetric_difference) (symmetric difference)##
select <list of columns> from tableA **full outer join** tableB B on A.key=B.key where A.key is null or B.key is null

1. A ∆ B = (A ∪ B) - (A ∩ B)
2. A ∆ B = (A - B) ∪ (B - A)


    A = {31, 33, 34, 39}
    B = {31, 33, 34, 35}
    A ∆ B = {39, 35}

    MySQL

    select <list of columns> from tableA A left join tableB B on A.key=B.key where B.key is null
        union
    select <list of columns> from tableA A right join tableB B on A.key=B.key where A.key is null



##References##
1. [A Visual Explanation of SQL Joins](https://plus.google.com/111053008130113715119/posts/5TdpUKQXxpu)
2. [SQL Joins (left, inner, right, outer): 7 Venn Diagrams (approx)](https://plus.google.com/111053008130113715119/posts/5TdpUKQXxpu)
3. [Set Theory Basics and Notation](http://en.wikipedia.org/wiki/Set_theory#Basic_concepts_and_notation)
4. [Set Theory Sets Notation](http://en.wikibooks.org/wiki/Set_Theory/Sets#Notation)
